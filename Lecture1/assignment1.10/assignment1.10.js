const lastName = "Doe";
let age = 33; 
const isDoctor = false;
const sender = "John";

let nextAge = age + 1;

let title = isDoctor ? "Dr." : "Mx.";

console.log(
  `
Dear ${title} ${lastName}

Congratulations on your ${nextAge} birthday! Many happy returns!

Sincerely,
${sender}
`
);

// kaikki muuttujat voi olla const tässä ohjelmassa