const exampleString = "word";
const exampleNum = 10;
let exampleBool = true;

console.log(exampleString + " " + typeof exampleString);
console.log(exampleNum + " " + typeof exampleNum);
console.log(exampleBool + " " + typeof exampleBool);

exampleBool = false;

console.log(exampleString + " " + typeof exampleString);
console.log(exampleNum + " " + typeof exampleNum);
console.log(exampleBool + " " + typeof exampleBool);
