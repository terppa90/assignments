const a = 5;
const b = 10;

/* EXTRA 2 */
// const a: number = Number(process.argv[2])
// const b: number = Number(process.argv[3])
// if (Number.isNaN(a) || Number.isNaN(b)) {
//     return console.log('This program requires two numeric parameters!')
// }

const sum = a + b;
const difference = a - b;
const fraction = a / b;
const product = a * b;

console.log(`Sum: ${a} + ${b} = ${sum}`);
console.log(`Difference: ${a} - ${b} = ${difference}`);
console.log(`Fraction: ${a} / ${b} = ${fraction}`);
console.log(`Product: ${a} * ${b} = ${product}`);

/* EXTRA */
const exponential = a ** b;
const modulo = a % b;

console.log(`The exponential of ${a} and ${b} is ${exponential}`);
console.log(`The modulo of ${a} and ${b} is ${modulo}`);

const average = sum / 2;
console.log(`The average of ${a} and ${b} is ${average}`);
