const str1 = "tero";
const str2 = "kaihula";
const strSum = str1 + str2;
const avgLength = (str1.length + str2.length) / 2;

// 1
console.log(strSum);
// 2
console.log(
  `str1 length: ${str1.length} str2 length: ${str2.length} strSum length: ${strSum.length}`
);
// 3
console.log(avgLength);
// 4
console.log(str1.toUpperCase());
// 5
console.log(`${str2[0]} ${str2.length - 1}`);
