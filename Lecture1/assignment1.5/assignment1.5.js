const playerCount = 4;

if (playerCount === 4) {
  console.log("Let the game begin.");
}

const isStressed = false;
const hasIcecream = true;

if (!isStressed || hasIcecream) {
  console.log("Mark is happy.");
} else {
  console.log("Mark is not happy.");
}

const sunIsShining = true;
const notRaining = true;
const temperature = 21;

if (sunIsShining && notRaining && temperature >= 20) {
  console.log("It's a beach day.");
} else {
  console.log("It is not a beach day");
}

const seesSuzy = false;
const seesDan = false;
const weekday = "Tuesday";

if (
  weekday === "Tuesday" &&
  ((arinSeesDan && !arinSeesSuzy) || (arinSeesSuzy && !arinSeesDan))
) {
  console.log("Arin is happy");
} else {
  console.log("Arin is not happy");
}
