let aString = process.argv[2] || "Jotain tekstiä  ";

aString = aString.trim();
aString = aString.substring(0, 20);
aString = aString.charAt(0).toLowerCase() + aString.substring(1);

console.log(aString);

/* EXTRA */
// const aString = 'Silloin Kauhavan rauta ruostuu ja Vaasankin veri vapisee     '
// if (aString !== aString.trim()) {
//     console.log('Invalid string: surrounding whitespace')
// } else if (aString.length > 20) {
//     console.error('Invalid string: exceeding maximum length (20)')
// } else if (aString.charAt(0).toLowerCase() !== aString.charAt(0)) {
//     console.error('Invalid string: starts with a capital letter')
// } else {
//     console.log(aString)
// }
