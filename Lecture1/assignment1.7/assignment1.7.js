const originalPrice = 10.0;
const discountPercentage = 20;
let discountedPrice =
  originalPrice - (originalPrice * discountPercentage) / 100;

console.log(
  `Original price: ${originalPrice}€\nDiscount percentage: ${discountPercentage}%\nDiscounted price: ${discountedPrice}€`
);
