// distance in kilometers and speed km/h
const distance = 100;
const speed = 50;

const travelTime = distance / speed;

console.log(`It takes ${travelTime} hours to travel.`);
