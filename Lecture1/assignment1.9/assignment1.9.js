const days = 365;
const hours = days * 24;
const minutes = hours * 60;
const secondsInYear = minutes * 60;

console.log(`There is ${secondsInYear} seconds in a year.`);
