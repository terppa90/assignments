import { Request, Response, NextFunction } from "express";
import jwt from "jsonwebtoken";

interface CustomRequest extends Request {
    username?: string | jwt.JwtPayload
}

export const secretEndpoint = (req: CustomRequest, res: Response, next: NextFunction) => {
	const auth = req.headers["authorization"];
	console.log(auth);
	
	if (!auth?.startsWith("Bearer ")) {
		return res.status(401).send("Invalid token");
	}
	
	const token = auth.substring(7);
	const secret = process.env.SECRET ?? "";
	try {
		const decodedToken = jwt.verify(token, secret);
		console.log(decodedToken);
		
		req.username = decodedToken;
		next();
	} catch (error) {
		return res.status(401).send("Invalid token");
	}
};

export const unknownEndpoint = (_req: Request, res: Response) => {
	res.status(404).send({ error: "404 Not found"});
};