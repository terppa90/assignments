import express from "express";
import "dotenv/config";
import notesRouter from "./notesRouter";
import usersRouter from "./usersRouter";

// Import middlewares
import { unknownEndpoint } from "./middlewares";

const server = express();
server.use(express.json());

// Routes
server.use("/api/v1/notes", notesRouter);
server.use("/api/v1/users", usersRouter);

server.use(unknownEndpoint);

export default server;