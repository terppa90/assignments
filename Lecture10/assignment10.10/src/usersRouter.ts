import express, { Request, Response } from "express";
import argon2 from "argon2";
import jwt from "jsonwebtoken";
import "dotenv/config";

const router = express.Router();

// Login with basic auth
router.post("/login", async (req: Request, res: Response) => {
	const { USERNAME_, PASSWORD_ } = process.env;

	if (USERNAME_ === undefined || PASSWORD_ === undefined) {
		return res.status(401).send("Invalid username or password");
	}

	// check for basic auth header
	if (!req.headers.authorization || req.headers.authorization.indexOf("Basic ") === -1) {
		return res.status(401).json({ message: "Missing Authorization Header" });
	}

	// verify auth credentials
	const base64Credentials =  req.headers.authorization.split(" ")[1];
	const credentials = Buffer.from(base64Credentials, "base64").toString("ascii");
	const [username, password] = credentials.split(":");
	const user = { username, password };
	console.log(user);

	if (user.username !== USERNAME_) {
		return res.status(401).json({ message: "Invalid username" });
	}

	if (!user) {
		return res.status(401).json({ message: "Invalid Authentication Credentials" });
	}

	if (user.username === USERNAME_) {
		const isValidPassword = await argon2.verify(PASSWORD_, password);
		if (!isValidPassword) {
			return res.status(401).send("Invalid password");
		}

		const payload = { username: username };
		const secret = process.env.SECRET ?? "";
		const options = { expiresIn: "15min"};

		const encodedToken = jwt.sign(payload, secret, options);
		// console.log(encodedToken);
		return res.status(201).send({message: "Login succesful", token: encodedToken});
	}
});

export default router;