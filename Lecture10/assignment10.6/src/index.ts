import express, {Request, Response} from "express";

const server = express();

server.use(express.json());
server.use(express.static("public"));


server.get("/hello", (req: Request, res: Response) => {
	res.send("Hello from docker");
});

const port = 3000;
server.listen(port, () => {
	console.log("Server listening port", port);
});