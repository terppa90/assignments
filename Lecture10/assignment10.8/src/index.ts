let num1: number = 0;
let num2: number = 1;
let temp;

function fibonacciNumbers(startNum: number, rounds: number) {
	let currentRound = startNum;

	function go() {
		console.log(num1);
		if (currentRound == rounds) {
			clearInterval(timerId);
		}
		currentRound++;
		temp = num1 + num2;
		num1 = num2;
		num2 = temp;
		
	}
	go();
	const timerId = setInterval(go, 1000);
}
fibonacciNumbers(0, 8);