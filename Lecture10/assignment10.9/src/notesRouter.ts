import express, { Request, Response } from "express";

const router = express.Router();

interface Note {
	id: number,
	name: string,
	author: string,
	note: string
}

const notes: Note[] = [];

router.post("/", (req: Request, res: Response) => {
	const note: Note = req.body;

	notes.push(note);

	res.send(req.body);
});

// Get all notes
router.get("/", (req: Request, res: Response) => {
	const getAllNotes = notes.map((note => ({
		...note
	})));
	res.send(getAllNotes);
});

// Get note by id
router.get("/:id", (req: Request, res: Response) => {
	const id = Number(req.params.id);

	const getNoteById = notes.find(note => note.id === id);

	if (getNoteById) {
		res.send(getNoteById);
	} else {
		res.status(404).json({ message: "Book not found" });
	}
});

// Delete a note by id
router.delete("/:id", (req: Request, res: Response) => {
	const id = Number(req.params.id);

	const index = notes.findIndex(note => note.id === id);

	if (index !== -1) {
		notes.splice(index, 1);
		res.json({ message: "Note deleted successfully" });
	} else {
		res.status(404).json({ message: "Note not found" });
	}
});

export default router;