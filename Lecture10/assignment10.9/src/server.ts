import express from "express";
import notesRouter from "./notesRouter";

// Import middlewares
import { unknownEndpoint } from "./middlewares";

const server = express();
server.use(express.json());

// Routes
server.use("/api/v1/notes", notesRouter);

server.use(unknownEndpoint);

export default server;