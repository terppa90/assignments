import express, { Request, Response } from "express";

const server = express();
server.use(express.json());

server.get("/", (_req: Request, res: Response) => {
	res.send("OK:UPDATE");
});

const port = process.env.PORT || 3000;
server.listen(port, () => {
	console.log("Server listening port", port);
});