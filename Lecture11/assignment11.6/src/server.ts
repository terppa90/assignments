import express from "express";
import eventsRouter from "./eventsRouter";

const server = express();
server.use(express.json());

// Routes
server.use("/", eventsRouter);

export default server;