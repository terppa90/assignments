import express, { Request, Response } from "express";
import eventsRouter from "./eventsRouter";

const server = express();
server.use(express.json());

// Routes
server.use("/events", eventsRouter);

server.get("/", (_req: Request, res: Response) => {
	res.send("Test");
});

export default server;