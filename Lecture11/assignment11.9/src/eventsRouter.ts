import express, { Request, Response } from "express";

const router = express.Router();

interface Event {
	id: number,
	title: string,
	description: string,
	date: string,
	time?: string
}

const events: Event[] = [];

router.get("/", (_req: Request, res: Response) => {
	const getAllEvents = events.map((event => ({
		...event
	})));
	res.send(getAllEvents);
});
// Get events by month
router.get("/:monthNumber", (req: Request, res: Response) => {
	const month = req.params.monthNumber;
	console.log(month);

	const getAllEventsByMonth = events.filter((event) => {
		// When date is YYYY:MM:DD, use substring to get the month number
		const date = event.date.substring(6, 7);
		console.log(date);
		if (date === month) {
			return { ...event };
		}
	});

	res.send(getAllEventsByMonth);
});

router.post("/", (req: Request, res: Response) => {
	const note: Event = req.body;

	events.push(note);

	res.send(req.body);
});

router.put("/:id", (req: Request, res: Response) => {
	const eventId = Number(req.params.id);
	const changes = req.body;

	const index = events.findIndex(event => event.id === eventId);

	if (index !== -1) {
		events[index] = changes;
		// res.json({ message: "Event edited successfully" });
		res.status(204).send();
	} else {
		res.status(404).json( { message: "Event does not exist"} );
	}

});

router.delete("/:id", (req: Request, res: Response) => {
	const id = Number(req.params.id);

	const index = events.findIndex(event => event.id === id);

	if (index !== -1) {
		events.splice(index, 1);
		res.json({ message: "Event deleted successfully" });
	} else {
		res.status(404).json({ message: "Event not found" });
	}
});

export default router;