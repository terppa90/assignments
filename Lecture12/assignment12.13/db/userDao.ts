import { executeQuery } from '../src/db'
import * as queries from './queries'

interface User {
    username: string,
    full_name: string,
    email: string
}

const insertUser = async (user: User) => {
    const params = [...Object.values(user)]
    
    console.log(`Inserting a new user ${params[0]}...`)
    const result = await executeQuery(queries.insertUser, params)
    console.log(`New user inserted succesfully.`)
    return result
}

const findAllUsers = async () => {
    console.log('Requesting for all users...')
    const result = await executeQuery(queries.findAllUsers)
    console.log(`Found ${result.rows.length} users.`)
    return result
}

const findOneUser = async (id: any) => {
    console.log(`Requesting a user with id: ${id}...`)
    const result = await executeQuery(queries.findOneUser, [id])
    console.log(`Found ${result.rows.length} user.`)
    return result
}

export default { insertUser, findAllUsers, findOneUser }