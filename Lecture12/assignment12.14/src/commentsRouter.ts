import { Router } from "express"
import dao from './db/commentsDao'

interface Comment {
    author: string,
    content: string
}

const router = Router()

router.get('/', async (req, res) => {
    const result = await dao.findAllComments()
    res.send(result.rows)
})

router.get('/:userID', async (req, res) => {
    const result = await dao.allCommentsOfOneUser(req.params.userID)
    res.send(result.rows)
})

router.post('/', async (req, res) => {
    const comment: Comment = req.body;
    await dao.insertComment(comment)

    res.send({ message: "New comment created succesfully." })
})

export default router