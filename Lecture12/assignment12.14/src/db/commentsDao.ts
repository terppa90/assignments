import { executeQuery } from '../db'
import * as queries from './queries'

interface Comment {
    author: string,
    content: string
}

const findAllComments = async () => {
    console.log('Requesting for all comments...')
    const result = await executeQuery(queries.findAllComments)
    console.log(`Found ${result.rows.length} comments.`)
    return result
}

const allCommentsOfOneUser = async (id: any) => {
    console.log(`Requesting all comments with id: ${id}...`)
    const result = await executeQuery(queries.allCommentsOfOneUser, [id])
    console.log(`Found ${result.rows.length} comments.`)
    return result
}

const insertComment = async (comment: Comment) => {
    const params = [...Object.values(comment)]
    console.log(`Inserting a new comment ${params[0]}...`)
    const result = await executeQuery(queries.insertComment, params)
    console.log(`New comment inserted succesfully.`)
    return result
}

export default { allCommentsOfOneUser, findAllComments, insertComment }