import { Router } from "express"
import express, { Request, Response } from "express"
import dao from './db/userDao'

const router = Router()

interface User {
    username: string,
    full_name: string,
    email: string
}

router.get('/', async (_req, res) => {
    const result = await dao.findAllUsers()
    res.send(result.rows)
})

router.post('/', async (req: Request, res: Response) => {
    const user: User = req.body;
    console.log(user);
    const result = await dao.insertUser(user)

    res.send({message: "New user created succesfully.", user: user.username})
})

router.get('/:id', async (req, res) => {
    const result = await dao.findOneUser(req.params.id)
    const product = result.rows[0]
    res.send(product)
})

export default router