import { executeQuery } from '../src/db'
import * as queries from './queries'

interface Post {
    title: string,
    content: string
}

const insertPost = async (post: Post) => {
    const params = [...Object.values(post)]
    console.log(`Inserting a new post ${params[0]}...`)
    const result = await executeQuery(queries.insertPost, params)
    console.log(`New post inserted succesfully.`)
    return result
}

const findAllPosts = async () => {
    console.log('Requesting for all posts...')
    const result = await executeQuery(queries.findAllPosts)
    console.log(`Found ${result.rows.length} posts.`)
    return result
}

const allCommentsOfOnePost = async (id: any) => {
    console.log(`Requesting a post with id: ${id}...`)
    const result = await executeQuery(queries.allCommentsOfOnePost, [id])
    console.log(`Found ${result.rows.length} post.`)
    return result
}

const deletePostById = async (id: any) => {
    const params = [id]
    const result = await executeQuery(queries.deletePostById, params)
    return result
}

export default { insertPost, findAllPosts, allCommentsOfOnePost, deletePostById }