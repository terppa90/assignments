// /users queries
export const insertUser = 'INSERT INTO users (username, full_name, email) VALUES ($1, $2, $3);'

export const findAllUsers = 'SELECT user_id, username FROM users;'

export const findOneUser = 'SELECT * FROM users WHERE user_id = $1;'

export const deleteUserById = 'DELETE FROM users WHERE user_id = $1;'

// /posts queries
export const insertPost = 'INSERT INTO posts (user_id, title, content, post_date) VALUES ($1, $2, $3, NOW());'

export const findAllPosts = 'SELECT post_id, user_id, title FROM posts;'

export const allCommentsOfOnePost = 'SELECT p.post_id, p.user_id, p.title, p.content, p.post_date, c.author AS "comment author", c.content AS "comment content" FROM posts AS p JOIN comments AS c ON p.post_id = c.post_id WHERE p.post_id=$1'

export const deletePostById = 'DELETE FROM posts WHERE post_id = $1;'

// /comments queries
export const findAllComments = 'SELECT * FROM comments;'

export const allCommentsOfOneUser = 'SELECT * FROM comments WHERE user_id = $1'

export const insertComment = 'INSERT INTO comments (post_id, user_id, author, content, comment_date) VALUES ($1, $2, $3, $4, NOW());'

export const deleteCommentById = 'DELETE FROM comments WHERE comment_id = $1;'