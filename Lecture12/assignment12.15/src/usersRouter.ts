import { Router } from "express"
import { Request, Response } from "express"
import dao from '../db/userDao'

const router = Router()

interface User {
    username: string,
    full_name: string,
    email: string
}

router.get('/', async (_req, res) => {
    const result = await dao.findAllUsers()
    res.send(result.rows)
})

router.post('/', async (req: Request, res: Response) => {
    const user: User = req.body
    await dao.insertUser(user)

    res.send({message: "New user created succesfully.", user: user.username})
})

router.get('/:id', async (req, res) => {
    const result = await dao.findOneUser(req.params.id)
    const product = result.rows[0]
    res.send(product)
})

router.delete('/:id', async (req, res) => {
    const id = req.params.id
    await dao.deleteUserById(id)
    res.status(200).send(`User with id ${id} deleted succesfully.`)
})

export default router