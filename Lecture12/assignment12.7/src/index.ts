import express from 'express'
import { createProductsTable } from './db'
import productRouter from './productRouter';

const server = express()

server.use("/", productRouter);

createProductsTable()

const { PORT } = process.env
server.listen(PORT, () => {
    console.log('Products API listening to port', PORT)
})