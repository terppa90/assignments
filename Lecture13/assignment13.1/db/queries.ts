export const getProducts = 'SELECT * FROM products;'

export const getProduct = 'SELECT * FROM products WHERE id = $1;'

export const addProduct = 'INSERT INTO products (name, price) VALUES ($1, $2);'

export const setProduct = 'UPDATE products SET name=$1, price=$2 WHERE id=$3;'

export const delProduct = 'DELETE FROM products WHERE id = $1;'