FROM node:21-alpine AS builder
WORKDIR /tk-forum-api

COPY package*.json ./
COPY ./src ./src
COPY ./tsconfig.json ./tsconfig.json

RUN npm ci
RUN npm run build

FROM node:21-alpine AS final

ARG PORT
ARG PG_HOST
ARG PG_PORT
ARG PG_USERNAME
ARG PG_PASSWORD
ARG PG_DATABASE
ARG NODE_ENV

ENV PORT=${PORT}
ENV PG_HOST=${PG_HOST}
ENV PG_PORT=${PG_PORT}
ENV PG_USERNAME=${PG_USERNAME}
ENV PG_PASSWORD=${PG_PASSWORD}
ENV PG_DATABASE=${PG_DATABASE}
ENV NODE_ENV=${NODE_ENV}

WORKDIR /tk-forum-api
COPY --from=builder ./tk-forum-api/dist ./dist
COPY ./package*.json ./
RUN npm ci --production

EXPOSE 3000
CMD ["npm", "start"]