import pg from 'pg'
// import 'dotenv/config'

const { PG_HOST, PG_PORT, PG_USERNAME, PG_PASSWORD, PG_DATABASE, NODE_ENV } = process.env

export const pool = new pg.Pool({
    host: PG_HOST,
    port: Number(PG_PORT),
    user: PG_USERNAME,
    password: PG_PASSWORD,
    database: PG_DATABASE,
    ssl: NODE_ENV === 'production'
})

export const executeQuery = async (query: string, parameters?: Array<any>) => {
    const client = await pool.connect()
    try {
        const result = await client.query(query, parameters)
        return result
    } catch (error: any) {
        console.error(error.stack)
        error.name = 'dbError'
        throw error
    } finally {
        client.release()
    }
}

export const createUsersTable = async () => {
    const query = `
        CREATE TABLE IF NOT EXISTS "users" (
            "user_id" SERIAL PRIMARY KEY,
            "username" varchar NOT NULL,
            "full_name" varchar NOT NULL,
            "email" varchar UNIQUE NOT NULL
        )`
    await executeQuery(query)
    console.log('Users table initialized')
}

export const createPostsTable = async () => {
    const query = `
        CREATE TABLE IF NOT EXISTS "posts" (
            "post_id" SERIAL PRIMARY KEY,
            "user_id" INT NOT NULL,
            "title" varchar NOT NULL,
            "content" varchar NOT NULL,
            "post_date" date NOT NULL,
            FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE
        )`
    await executeQuery(query)
    console.log('Posts table initialized')
}

export const createCommentsTable = async () => {
    const query = `
            CREATE TABLE IF NOT EXISTS "comments" (
                "comment_id" SERIAL PRIMARY KEY,
                "post_id" INT NOT NULL,
                "user_id" INT NOT NULL,
                "author" varchar NOT NULL,
                "content" varchar NOT NULL,
                "comment_date" date NOT NULL,
                FOREIGN KEY (post_id) REFERENCES posts(post_id) ON DELETE CASCADE,
                FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE
            )`
    await executeQuery(query)
    console.log('Comments table initialized')
}
