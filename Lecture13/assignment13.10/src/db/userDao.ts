import { executeQuery } from '../db'
import * as queries from './queries'

interface User {
    username: string,
    full_name: string,
    email: string
}

const insertUser = async (user: User) => {
    const params = [...Object.values(user)]
    console.log(params);
    
    console.log(`Inserting a new user ${params[0]}...`)
    const result = await executeQuery(queries.insertUser, params)
    console.log(`New user inserted succesfully.`)
    return result;
}

const findAllUsers = async () => {
    console.log('Requesting for all users...')
    const result = await executeQuery(queries.findAllUsers)
    console.log(`Found ${result.rows.length} users.`)
    return result
}

const findOneUser = async (id: any) => {
    console.log(`Requesting a user with id: ${id}...`)
    const result = await executeQuery(queries.findOneUser, [id])
    console.log(`Found ${result.rows.length} user.`)
    return result
}

const deleteUserById = async (id: any) => {
    const params = [id]
    const result = await executeQuery(queries.deleteUserById, params)
    return result
}

const UpdateUserById = async (user: any) => {
    const params = [ user.username, user.full_name, user.email, user.user_id ]
    console.log(`Updating a user "${params[0]}"...`)
    const result = await executeQuery(queries.UpdateUserById, params)
    console.log(`User ${user.username} updated succesfully.`)
    return result
}


export default { insertUser, findAllUsers, findOneUser, deleteUserById, UpdateUserById }