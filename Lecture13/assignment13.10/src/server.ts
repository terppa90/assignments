import express from 'express'
import { createUsersTable, createPostsTable, createCommentsTable } from './db'
import usersRouter from './usersRouter'
import postsRouter from './postsRouter'
import commentsRouter from './commentsRouter'

export const server = express()
server.use(express.json())

server.use("/users", usersRouter)
server.use("/posts", postsRouter)
server.use("/comments", commentsRouter)

createUsersTable()
createPostsTable()
createCommentsTable()