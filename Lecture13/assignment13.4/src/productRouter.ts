import express, { Request, Response } from 'express'
import { getProducts, getProduct, addProduct, delProduct, setProduct } from './db/productDao'

const router = express.Router()

router.get('/', async (req: Request, res: Response) => {
    const products = await getProducts()
    res.send(products)
})

router.get('/:id', async (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const product = await getProduct(id)
    res.send(product)
})

router.post('/', async (req: Request, res: Response) => {
    const { name, price } = req.body
    const result = await addProduct(name, price)
    res.send(result)
})

router.put('/:id', async (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const { name, price } = req.body
    const result = await setProduct(id, name, price)
    res.send(result)
})

router.delete('/:id', async (req: Request, res: Response) => {
    const id = Number(req.params.id)
    await delProduct(id)
    res.send()
})

export default router