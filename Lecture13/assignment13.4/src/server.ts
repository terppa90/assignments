import express from 'express'
import { createProductsTable } from './db'
import productRouter from './productRouter';

export const server = express()

server.use("/products", productRouter);

createProductsTable()