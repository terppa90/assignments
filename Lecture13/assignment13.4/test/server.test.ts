const request = require('supertest');
import { jest } from '@jest/globals'
import { pool } from '../src/db'
import { server } from '../src/server'

const initializeMockPool = (mockResponse: any) => {
    (pool as any).connect = jest.fn(() => {
        return {
            query: () => mockResponse,
            release: () => null
        }
    })
}

test('dummy test', () => {
    expect(true).toBe(true)
})

describe('Testing GET /products', () => {
    const mockResponse = {
        rows: [
            { id: 1, name: 'Uusi tuote', price: 10 },
            { id: 2, name: 'Uusi tuote2', price: 20 }
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })

    it('should return all products', async () => {
        const response = await request(server).get('/')
        expect(response.statusCode).toBe(200)
        expect(response.body.length).toEqual(1)
        expect(response.body).toStrictEqual(mockResponse.rows)
    })
})
