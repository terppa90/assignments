import { executeQuery } from '../db'
import * as queries from './queries'

interface Comment {
    author: string,
    content: string
}

const findAllComments = async () => {
    console.log('Requesting for all comments...')
    const result = await executeQuery(queries.findAllComments)
    console.log(`Found ${result.rows.length} comments.`)
    return result
}

const allCommentsOfOneUser = async (id: any) => {
    console.log(`Requesting all comments with id: ${id}...`)
    const result = await executeQuery(queries.allCommentsOfOneUser, [id])
    console.log(`Found ${result.rows.length} comments.`)
    return result
}

const insertComment = async (comment: Comment) => {
    const params = [...Object.values(comment)]
    console.log(`Inserting a new comment ${params[0]}...`)
    const result = await executeQuery(queries.insertComment, params)
    console.log(`New comment inserted succesfully.`)
    return result;
}

const deleteCommentById = async (id: any) => {
    const params = [id]
    const result = await executeQuery(queries.deleteCommentById, params)
    return result
}

const UpdateCommentById = async (comment: any) => {
    const params = [ comment.author, comment.content, comment.comment_id ]
    console.log(`Updating a comment with id "${comment.comment_id}"...`)
    const result = await executeQuery(queries.UpdateCommentById, params)
    console.log(`Comment with id ${comment.comment_id} updated succesfully.`)
    return result
}

export default { allCommentsOfOneUser, findAllComments, insertComment, deleteCommentById, UpdateCommentById }