import { Router } from "express"
import dao from './db/postDao'

const router = Router()

interface Post {
    title: string,
    content: string,
}

router.post('/', async (req, res) => {
    const post: Post = req.body
    await dao.insertPost(post)

    res.send({message: "New post created succesfully.", post: post.title})
})

router.get('/', async (_req, res) => {
    const result = await dao.findAllPosts()
    res.send(result.rows)
})

router.get('/:id', async (req, res) => {
    const result = await dao.allCommentsOfOnePost(req.params.id)
    const product = result.rows
    res.send(product)
})

router.delete('/:id', async (req, res) => {
    const id = req.params.id
    await dao.deletePostById(id)
    res.status(200).send({ message: `Post with id ${id} deleted succesfully.`})
})

router.put('/:id', async (req, res) => {
    const post = { post_id: req.params.id, ...req.body }
    await dao.UpdatePostById(post)
    res.status(200).send({ message: `Post with id ${req.params.id} modified succesfully.`})
})

export default router