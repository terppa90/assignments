const request = require('supertest');
import { jest } from '@jest/globals'
import { pool } from '../src/db'
import { server } from '../src/server'

const initializeMockPool = (mockResponse: any) => {
    (pool as any).connect = jest.fn(() => {
        return {
            query: () => mockResponse,
            release: () => null
        }
    })
}

describe('Testing GET /users', () => {
    const mockResponse = {
        rows: [
            { user_id: 1, username: 'tete' },
            { user_id: 2, username: 'tate' }
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    it('Should return all users', async () => {
        const response = await request(server).get('/users')
        expect(response.statusCode).toBe(200)
        expect(response.body).toStrictEqual(mockResponse.rows)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })
})

describe('Testing GET /users/:id', () => {
    const id = 1;

    const mockResponse = {
        rows: [
            { user_id: 1, username: 'tete', full_name: "Teppo Testaaja", email: "teppo.testaaja@buutti.com"}
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    it('Should return one user', async () => {
        const response = await request(server).get(`/users/${id}`)
        expect(response.statusCode).toBe(200)
        expect(response.body).toStrictEqual(mockResponse.rows[0])
    })

    afterAll(() => {
        jest.clearAllMocks()
    })
})

describe('Testing POST /users', () => {
    const user = { username: 'Uusi', full_name: "Uusi Käyttäjä", email: "uusi.käyttäjä@buutti.com" }

    const mockResponse = { "message": "New user created succesfully.", "user": "Uusi" }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    it('Should create a new user', async () => {
        const response = await request(server).post(`/users`).send(user)
        expect(response.statusCode).toBe(200)
        expect(response.body).toStrictEqual(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })
})

describe('Testing PUT /users/:id', () => {
    const user = { username: 'Uusi', full_name: "Uusi Käyttäjä Muokattu", email: "uusi.käyttäjä@buutti.com" }
    const id = 1;

    const mockResponse = { message: `User with id ${id} modified succesfully.` }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    it('Should update a user', async () => {
        const response = await request(server).put(`/users/${id}`).send(user)
        expect(response.statusCode).toBe(200)
        expect(response.body).toStrictEqual(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })
})

describe("Testing DELETE /users/:id", () => {
    const user_id = 1;

    const mockResponse = { message: `User with id ${user_id} deleted succesfully.`}

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    it("Should delete a user", async () => {
        const response = await request(server).delete(`/users/${user_id}`)
        expect(response.statusCode).toBe(200)
        expect(response.body).toStrictEqual(mockResponse)
    });

    afterAll(() => {
        jest.clearAllMocks()
    })
});


// /posts TESTS

describe('Testing GET /posts', () => {
    const mockResponse = {
        rows: [
            { post_id: 1, user_id: 1, title: 'Otsikko1' },
            { post_id: 2, user_id: 2, title: 'Otsikko2' }
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    it('Should return all posts', async () => {
        const response = await request(server).get('/posts')
        expect(response.statusCode).toBe(200)
        expect(response.body).toStrictEqual(mockResponse.rows)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })
})

describe('Testing GET /posts/:id', () => {
    const id = 1;

    const mockResponse = {
        rows: [
            { post_id: 1, user_id: 1, title: 'Otsikko1', content: "Postaus1 sisältö", post_date: "2024-01-26T22:00:00.000Z", comment_author: "ouoh",
            comment_content: "Uusi sisältö" }
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    it('Should return all comments of one post', async () => {
        const response = await request(server).get(`/posts/${id}`)
        expect(response.statusCode).toBe(200)
        expect(response.body).toStrictEqual(mockResponse.rows)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })
})

describe('Testing POST /posts', () => {
    const post = { user_id: 1, title: "Uusi postaus", content: "Uusi postauksen sisältö" }

    const mockResponse = { message: "New post created succesfully.", post: post.title }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    it('Should create a new post', async () => {
        const response = await request(server).post(`/posts`).send(post)
        expect(response.statusCode).toBe(200)
        expect(response.body).toStrictEqual(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })
})

describe('Testing PUT /posts/:id', () => {
    const post = { title: "Muokattu postaus", content: "Muokattu postauksen sisältö" }
    const id = 1;

    const mockResponse = { message: `Post with id ${id} modified succesfully.`}

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    it('Should update a post', async () => {
        const response = await request(server).put(`/posts/${id}`).send(post)
        expect(response.statusCode).toBe(200)
        expect(response.body).toStrictEqual(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })
})

describe("Testing DELETE /posts/:id", () => {
    const post_id = 1;

    const mockResponse = { message: `Post with id ${post_id} deleted succesfully.`}

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    it("Should delete one post", async () => {
        const response = await request(server).delete(`/posts/${post_id}`)
        expect(response.statusCode).toBe(200)
        expect(response.body).toStrictEqual(mockResponse)
    });

    afterAll(() => {
        jest.clearAllMocks()
    })
});

// /comments TESTS

describe('Testing GET /comments', () => {
    const mockResponse = {
        rows: [
            { comment_id: 1, post_id: 1, user_id: 1, author: "tete", content: "Postaus1 sisältö", comment_date: "2024-01-26T22:00:00.000Z"},
            { comment_id: 2, post_id: 1, user_id: 1, author: "tete", content: "Postaus2 sisältö", comment_date: "2024-01-26T22:00:00.000Z"}
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    it('Should return all comments', async () => {
        const response = await request(server).get('/posts')
        expect(response.statusCode).toBe(200)
        expect(response.body).toStrictEqual(mockResponse.rows)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })
})

describe('Testing GET /comments/:id', () => {
    const id = 1;

    const mockResponse = {
        rows: [
            { comment_id: 1, post_id: 1, user_id: 1, author: "tete", content: "Postaus1 sisältö", comment_date: "2024-01-26T22:00:00.000Z" },
            { comment_id: 2, post_id: 2, user_id: 1, author: "tete", content: "Postaus2 sisältö", comment_date: "2024-01-26T22:00:00.000Z" },
            { comment_id: 3, post_id: 1, user_id: 1, author: "tete", content: "Postaus3 sisältö", comment_date: "2024-01-26T22:00:00.000Z" }
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    it('Should return all comments of one user', async () => {
        const response = await request(server).get(`/comments/${id}`)
        expect(response.statusCode).toBe(200)
        expect(response.body).toStrictEqual(mockResponse.rows)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })
})

describe('Testing POST /comments', () => {
    const post = { post_id: 1, user_id: 1, author: "tete", content: "Postaus1", comment_date: "2024-01-30T22:00:00.000Z" }

    const mockResponse = { message: "New comment created succesfully." }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    it('Should create a new comment', async () => {
        const response = await request(server).post(`/comments`).send(post)
        expect(response.statusCode).toBe(200)
        expect(response.body).toStrictEqual(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })
})

describe('Testing PUT /comments/:id', () => {
    const post = { author: "tete", content: "Postaus1 muokattu" }
    const id = 1;

    const mockResponse = { message: `Comment with id ${id} modified succesfully.`}

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    it('Should update a comment', async () => {
        const response = await request(server).put(`/comments/${id}`).send(post)
        expect(response.statusCode).toBe(200)
        expect(response.body).toStrictEqual(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })
})

describe("Testing DELETE /posts/:id", () => {
    const id = 1;

    const mockResponse = { message: `Comment with id ${id} deleted succesfully.`}

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    it("Should delete one comment", async () => {
        const response = await request(server).delete(`/comments/${id}`)
        expect(response.statusCode).toBe(200)
        expect(response.body).toStrictEqual(mockResponse)
    });

    afterAll(() => {
        jest.clearAllMocks()
    })
});

