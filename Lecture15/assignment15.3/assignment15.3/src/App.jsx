import RandomNumber from "./RandomNumber"

function App() {

  return (
    <div className='App'>
      <h1>Random number generator</h1>
      <RandomNumber />
    </div>
  )
}

export default App