const RandomNumber = () => {
    const randNum = Math.floor(Math.random() * 100) + 1
    return (
        <div className='RandomNumber'>
            Random number between 1-100: {randNum}
        </div>
    )
}

export default RandomNumber