import Instrument from "./Instrument";
import Guitar from "./assets/guitar.png"
import Violin from "./assets/violin.png"
import Tuba from "./assets/tuba.png"

function App() {
  return (
    <div className="App">
      <h1>Instruments</h1>

      <Instrument
        name="Guitar"
        image={Guitar}
        price={899}
      />

      <Instrument
        name="Violin"
        image={Violin}
        price={599}
      />

      <Instrument
        name="Tuba"
        image={Tuba}
        price={799}
      />
    </div>
  );
}

export default App;
