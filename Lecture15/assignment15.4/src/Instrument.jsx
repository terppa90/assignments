const Instrument = (props) => {
  return (
    <>
      <p>{props.name}</p>
      <img src={props.image} />
      <p>{props.price}</p>
    </>
  );
};

export default Instrument;
