import NameList from "./NameList"

function App() {
  const namelist = ["Ari", "Jari", "Kari", "Sari", "Mari", "Sakari", "Jouko"]

  return (
    <div className='App'>
      <NameList names={namelist} />
    </div>
  )
}

export default App