/* eslint-disable react/prop-types */

const NameList = (props) => {
  return (
    <>
      <div>
        <h1>List of names</h1>
        {props.names.map((person) => {
          return (
            <div key={person}>
              Name: <b>{person}</b>
            </div>
          )
        })}
      </div>
    </>
  )
}

export default NameList
