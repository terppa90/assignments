import Button from "./Button"

function App() {
  return (
    <div className="App">
      <h1>Count app</h1>
      <Button />
      <Button />
      <Button />
    </div>
  )
}

export default App
