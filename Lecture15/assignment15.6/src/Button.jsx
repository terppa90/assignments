import { useState } from "react"

const Button = () => {
  const [count, setCount] = useState(0)

  return (
    <div>
      <button onClick={() => setCount(count + 1)}>Increment: {count}</button>
    </div>
  )
}

export default Button
