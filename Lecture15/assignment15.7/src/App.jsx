import UserInput from "./UserInput"

function App() {

  return (
    <div className='App'>
      <h1>User Input</h1>
      <UserInput />
    </div>
  )
}

export default App