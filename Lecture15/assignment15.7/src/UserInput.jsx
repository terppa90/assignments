import {useState} from "react";

const UserInput = () => {
  const [userInput, setUserInput] = useState("");

  const onInputChange = (event) => {
    setUserInput(event.target.value)
}

function handleClick() {
    alert(`Your input was: ${userInput}`);
  }

  return (
    <div>
      <input value={userInput} onChange={onInputChange} />
      <button onClick={handleClick}>Button</button>

    </div>
  );
};

export default UserInput;
