import { useState } from "react";

const TodoList = () => {
  const [inputTask, setInputTask] = useState("");
  const [list, setList] = useState([]);

  const handleAddTodo = () => {
    const newTask = {
      id: Math.random(),
      todo: inputTask,
    };

    setList([...list, newTask]);
    setInputTask("");
  };

  const handleDeleteTodo = (id) => {
    const newList = list.filter((todo) => todo.id !== id);
    setList(newList);
  };

  const handleInputChange = (event) => {
    setInputTask(event.target.value);
  };

  const toggleTodo = (id) => {
    let updatedTodos = list.map((todo) => {
      if (todo.id === id) {
        todo.isComplete = !todo.isComplete;
      }
      return todo;
    });
    setList(updatedTodos);
  };

  return (
    <div className="todo">
      <h1>To-Do List</h1>
      <div>
        <input
          className="input"
          type="text"
          value={inputTask}
          onChange={handleInputChange}
          placeholder="Enter a task"
        />
        <button className="add-btn" onClick={() => handleAddTodo(inputTask)}>
          ADD
        </button>
      </div>

      <ul>
        {list.map((todo, index) => (
          <div
            className={todo.isComplete ? "todo-row-complete" : "todo-row"}
            key={index}
          >
            <li className="task" key={todo.id}>
              {todo.todo}
              <button className="btn" onClick={() => handleDeleteTodo(todo.id)}>
                Delete
              </button>
              <button className="btn" onClick={() => toggleTodo(todo.id)}>
                {todo.isComplete ? "Completed" : "Not completed"}
              </button>
            </li>
          </div>
        ))}
      </ul>
    </div>
  );
};

export default TodoList;
