/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-key */
import { useState } from "react";

interface InputFieldProps {
  addPhoneNum: (phoneNumber) => void
}

const InputField = (props) => {
  const [input, setInput] = useState("");

  // const sendInfo = () => {
  //   props.addPhoneNum(input);
  //   setInput("");
  // }

  const validateInput = (input) => {
    for (let i=0; i < input.length; i++) {
      const char = input[i];
      if (!"0123456789".includes(char)) {
        return false
      }
    }
    return true
  }

  const onInputChange = (event: ChangeEvent<HTML> InputElement>) => {
    const newInput = event.target.value;

    if (!validateInput(newInput)) {
      return
    }

    if (newInput.length === 10) {
      sendInfo()
    } else {
      setInput(event.target.value)
    }
  }
  
  return (
    <input
      type="text"
      value={input}
      onChange={onInputChange}
    />
  );
};

function App() {
  const [phoneNums, setPhoneNums] = useState<Array<string>>([]);

  const addPhoneNum = (phoneNumber) => {
    setPhoneNums(phoneNums.concat(phoneNumber));
  };

  return (
    <div className="App">
      {phoneNums.map(phoneNumer => {
        return <div>{phoneNumer}</div>
      })}
      <InputField addPhoneNum={addPhoneNum}/>
    </div>
  );
}

export default App;
