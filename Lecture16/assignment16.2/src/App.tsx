import React, { useEffect, useState } from 'react'

const App = () => {
  const [time, setTime] = useState(0);

  const clearTime = () => {
    console.log("Reset works");
  }

  useEffect(() => {
    const timeout = setTimeout(() => {
      setTime((time) => time + 1);
    }, 1000)

    return () => clearTimeout(timeout);
  }, [time]);

  return (
    <>
      <div>
        <h1>Timer</h1>
        {time}
        <br />
      <button onClick={clearTime}>Reset time</button>
      </div>
    </>
  )
}

export default App