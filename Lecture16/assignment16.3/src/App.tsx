import { useState, useEffect } from "react";
import axios from "axios";

const App = () => {
  const [joke, setJoke] = useState("Not fetched yet");

    useEffect(() => {
      async function fetchDadJokes() {
        const url = 'https://api.api-ninjas.com/v1/dadjokes'
        const config = {
          headers: {
            'X-Api-Key': 'qFRwfcoEjiLRvRYhJirWtQ==1JTjQy3ah2MoAPgM'
          }
        }
        const response = await axios.get(url, config)
        // console.log(response);
        
        const data = response.data[0].joke
        setJoke(data)
      }
      fetchDadJokes();
    }, []);

  return (
    <div>{joke}</div>
  )
}

export default App