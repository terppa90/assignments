import { useState } from 'react'
import './App.css';

const App = () => {
  const [numList, setNumList] = useState<Array<number>>([]);

  const addNumber = () => {
    let newBingoNum = Math.floor(Math.random() * 75) + 1;

    while (numList.includes(newBingoNum)) {
      newBingoNum = Math.floor(Math.random() * 75) + 1;
    }

    setNumList([...numList, newBingoNum])
  }

  return (
    <div className='bingo'><h1>Bingo App</h1>
      <ul>
        {numList.map((num: number) =>
          <li><p className='balls'>{num}</p></li>
        )}
      </ul>
      <button onClick={addNumber}>+</button>
    </div>
  )
}

export default App