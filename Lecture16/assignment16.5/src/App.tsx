import { useState } from 'react'

const App = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [selectedValue, setSelectedValue] = useState(null);

  const onOptionChange = (e: any) => {
    setSelectedValue(e.target.value);
  }

  const onChangeName = (e: any) => {
    setName(e.target.value);
  }

  const onChangeEmail = (e: any) => {
    setEmail(e.target.value);
  }

  const onSubmit = (e: any) => {
    // alert("Form submitted");
    console.log(e.target)
    e.preventDefault();
  }

  const resetForm = (e: any) => {
    e.target.reset();
  }

  return (
    <div>
      <h1>Feedback</h1>
      <form onSubmit={onSubmit}>
        <div className="radio">
          <h3>Choose feedback type:</h3>
          <label>
            <input type="radio" value="feedback" checked={selectedValue==="feedback"} onChange={onOptionChange} />
            Feedback
          </label>
          <label>
            <input type="radio" value="suggestion" checked={selectedValue==="suggestion"} onChange={onOptionChange} />
            Suggestion
          </label>
          <label>
            <input type="radio" value="question" checked={selectedValue==="question"} onChange={onOptionChange} />
            Question
          </label>
        </div>
        <div className='feedback'>
          <label>
            <p>Write your feedback:</p>
            <textarea name="postContent" rows={4} cols={40} />
          </label>
        </div>
        <br />
        <label>Enter your name: 
        <input className='input' name={name} type='text' value={name} onChange={onChangeName}/>
        </label>
        <br />
        <label>Enter your email:
        <input className='input' name={email} type='text' value={email} onChange={onChangeEmail}/>
        </label>
        <br /><br />
        <button type="reset" value="reset">Reset</button>
        <button type="submit" value="Submit">Send</button>
      </form>
    </div>
  )
}

export default App