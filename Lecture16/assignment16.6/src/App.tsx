/* eslint-disable @typescript-eslint/no-explicit-any */
import { useState, useEffect } from "react";

const App = () => {
  const [catPic, setCatPic] = useState("");
  const [catPicWithText, setCatPicWithText] = useState("");
  const [text, setText] = useState("");
  const [toggleCat, setToggleCat] = useState(true);

  const onSubmit = (e: any) => {
    if (text.length > 0) {
      const url = `https://cataas.com/cat/says/${text}`
      setCatPicWithText(url)
      setToggleCat(false)
      e.preventDefault();
    }
  }

  const onChangeText = (e: any) => {
    setText(e.target.value)
  }

  useEffect(() => {
    function loadCatPics() {
      const url = `https://cataas.com/cat`
      setCatPic(url)
    }
    loadCatPics();
  }, []);

  return (
    <form onSubmit={onSubmit}>
      {toggleCat ? (<img src={catPic}></img>) : (<img src={catPicWithText}></img>)}
      <br />
      <input type="text" name={text} value={text} onChange={onChangeText} />
      <br />
      <button type="submit" value="submit">Reload</button>
    </form>
  )
}

export default App