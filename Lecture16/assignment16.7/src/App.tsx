/* eslint-disable @typescript-eslint/no-explicit-any */
// @ts-nocheck
import { useState, useEffect } from 'react'
import './app.css'
import SearchBar from './SearchBar';

const App = () => {
  const [formData, setFormData] = useState({
    name: "",
    email: "",
    phone: "",
    address: "",
    website: "",
    notes: "",
  })
  const [contactList, setContactList] = useState([formData]);
  const [showForm, setShowForm] = useState(false);
  const [hideContactList, setHideContactList] = useState(false);
  const [showDetails, setShowDetails] = useState(false);
  const [filteredData, setFilteredData] = useState(contactList);
  const [showOneContact, setShowOneContact] = useState({});
  const [showEditContact, setShowEditContact] = useState(false);

  useEffect(() => {
    setContactList([{name: "John Doe", email: "johndoe@gmail.com", phone: "0441233321", address: "Some Address", website: "johndoe.com", notes: "Woohoo"}])
  }, [])

  const handleCancel = () => {
    setFormData({
      name: "",
      email: "",
      phone: "",
      address: "",
      website: "",
      notes: "",
    })
    setShowForm(false)
    setShowEditContact(false)
  };

  const addContact = () => {
    setShowForm(true)
    setShowDetails(false)
  }

  const showContactDetails = (contact: any) => {
    setShowDetails(true)
    setShowForm(false)
    setShowOneContact(contact)
    setShowEditContact(false)
  }

  const removeContact = (contactToRemove: any) => {
    const newContacts = contactList.filter((contact) => contact.name !== contactToRemove)
    console.log(newContacts);
    
    setContactList(newContacts)
    setShowDetails(false)
  }

  const editContact = () => {
    setShowEditContact(true)
    setShowDetails(false)
  }

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    setContactList([...contactList, formData])
  }

  const handleEditSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    console.log("Edit contact");
    
  }

  return (
    <>
      <div className='left'>
        <SearchBar
          contactList={contactList}
          setFilteredData={setFilteredData}
          setHideContactList={setHideContactList}
        />
        <div>
          <h3>Search result:</h3>
          {filteredData.map((contact) => {
            return <div key={contact.name} onClick={() => showContactDetails(contact)}>
              <div>{contact.name}</div>
            </div>
          })}
        </div>
        <div>
          <h3>Contacts:</h3>
          {contactList.map((contact) => {
            return <div key={contact.name} onClick={() => showContactDetails(contact)}>{contact.name}</div>
          })}
        </div>
        <button className='button' onClick={() => addContact()}>Add Contact</button>
      </div>
      {showForm && (
        <>
          <div className='wrapper'>
            <form className='right' onSubmit={handleSubmit}>
              <h2>Add a new contact</h2>
              <label>Name</label>
              <br />
              <input name={formData.name} type='text' value={formData.name} onChange={(e) => setFormData({ ...formData, name: e.target.value })} />
              <br />
              <label>Email address</label>
              <br />
              <input name={formData.email} type='text' value={formData.email} onChange={(e) => setFormData({ ...formData, email: e.target.value })} />
              <br />
              <label>Phone number</label>
              <br />
              <input name={formData.phone} type='text' value={formData.phone} onChange={(e) => setFormData({ ...formData, phone: e.target.value })} />
              <br />
              <label>Address</label>
              <br />
              <input name={formData.address} type='text' value={formData.address} onChange={(e) => setFormData({ ...formData, address: e.target.value })} />
              <br />
              <label>Website</label>
              <br />
              <input name={formData.website} type='text' value={formData.website} onChange={(e) => setFormData({ ...formData, website: e.target.value })} />
              <br />
              <label>Notes</label>
              <br />
              <input name={formData.notes} type='text' value={formData.notes} onChange={(e) => setFormData({ ...formData, notes: e.target.value })} />
              <br />
              <button className='button' type='submit' value="Submit">Save</button>
              <button className='button' type='reset' onClick={handleCancel}>Cancel</button>
              <br />
              <br />
            </form>
          </div>
        </>
      )}
      <form className='right'>
        {showDetails && (
          <div>
            <h1>{showOneContact.name}</h1> <br />
            <i>Phone: </i>{showOneContact.phone} <br />
            <i>Address: </i>{showOneContact.address} <br />
            <i>Website: </i>{showOneContact.website} <br />
            <i>Notes: </i>{showOneContact.notes} <br />
            <button className='button' type="button" onClick={() => removeContact(showOneContact.name)}>Remove</button>
            <button className='button' type="button" onClick={() => editContact()}>Edit</button>
          </div>
        )}
      </form>
      {showEditContact && (
        <div>
          <form className='right' onSubmit={handleEditSubmit}>
            <h2>Edit contact</h2>
            <label>Name</label>
            <br />
            <input name={showOneContact.name} type='text' value={showOneContact.name} onChange={(e) => setShowOneContact({ ...showOneContact, name: e.target.value })} />
            <br />
            <label>Email address</label>
            <br />
            <input name={showOneContact.email} type='text' value={showOneContact.email} onChange={(e) => setShowOneContact({ ...showOneContact, email: e.target.value })} />
            <br />
            <label>Phone number</label>
            <br />
            <input name={showOneContact.phone} type='text' value={showOneContact.phone} onChange={(e) => setShowOneContact({ ...showOneContact, phone: e.target.value })} />
            <br />
            <label>Address</label>
            <br />
            <input name={showOneContact.address} type='text' value={showOneContact.address} onChange={(e) => setShowOneContact({ ...showOneContact, address: e.target.value })} />
            <br />
            <label>Website</label>
            <br />
            <input name={showOneContact.website} type='text' value={showOneContact.website} onChange={(e) => setShowOneContact({ ...showOneContact, website: e.target.value })} />
            <br />
            <label>Notes</label>
            <br />
            <input name={showOneContact.notes} type='text' value={showOneContact.notes} onChange={(e) => setShowOneContact({ ...showOneContact, notes: e.target.value })} />
            <br />
            <button className='button' type='submit' value="Submit">Save</button>
            <button className='button' type='reset' onClick={handleCancel}>Cancel</button>
            <br />
            <br />
          </form>
        </div>
      )}
    </>
  )
}

export default App