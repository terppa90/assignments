import { useState } from 'react'

const SearchBar = (props: any) => {
    const [searchQuery, setSearchQuery] = useState('');

    const handleSearchInputChange = (event: any) => {
        setSearchQuery(event.target.value);
      }
    
      const handleSearchSubmit = (event: any) => {
        event.preventDefault();
        console.log(`Searching for: ${searchQuery}`);
        const filteredData = props.contactList.filter((contact: any) => {
            return contact.name.toLowerCase().includes(searchQuery.toLowerCase());
          });
          props.setFilteredData(filteredData);
      }

    return (
        <form onSubmit={handleSearchSubmit}>
            <input
                type="text"
                placeholder="Search..."
                value={searchQuery}
                onChange={handleSearchInputChange}
            />
            <button onClick={props.setHideContactList(true)} type="submit">Search</button>
        </form>
    )
}

export default SearchBar