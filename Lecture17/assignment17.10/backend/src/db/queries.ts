// /users queries

export const findAllSongs = "SELECT id, title FROM songs;";

export const findOneSong = "SELECT * FROM songs WHERE title = $1;";

export const insertSong = "INSERT INTO songs (title, lyrics) VALUES ($1, $2);";