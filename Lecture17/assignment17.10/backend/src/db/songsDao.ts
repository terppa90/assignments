/* eslint-disable @typescript-eslint/no-explicit-any */
import { executeQuery } from "../db";
import * as queries from "./queries";

interface Song {
    title: string,
    lyrics: string
}

const findOneSong = async (title: any) => {
	console.log(`Requesting a song with title: ${title}...`);
	const result = await executeQuery(queries.findOneSong, [title]);
	console.log(`Found ${result.rows.length} song.`);
	return result.rows;
};

const findAllSongs = async () => {
	console.log("Requesting for all songs...");
	const result = await executeQuery(queries.findAllSongs);
	console.log(`Found ${result.rows.length} songs.`);
	return result;
};

const insertSong = async (song: Song) => {
	const params = [...Object.values(song)];
	console.log(params);
    
	console.log(`Inserting a new song ${params[0]}...`);
	const result = await executeQuery(queries.insertSong, params);
	console.log("New song inserted succesfully.");
	return result;
};

export default { findOneSong, findAllSongs, insertSong };