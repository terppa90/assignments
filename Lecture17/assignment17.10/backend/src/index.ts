import server from "./server";

// server.use('/', express.static('./dist/client'))

const port = process.env.PORT || 3000;
server.listen(port, () => {
	console.log("Server listening port", port);
});
