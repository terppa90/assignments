import express, { Request, Response } from "express";
import songsRouter from "./songsRouter";

const server = express();
server.use(express.json());

// Routes
server.use("/songs", songsRouter);

server.get("/", (_req: Request, res: Response) => {
	res.send("/GET works");
});

export default server;