import express, { Request, Response } from "express";
import { Song, songs } from "./songList";
import dao from "./db/songsDao";

const router = express.Router();

router.get('/', async (_req: Request, res: Response) => {
    const result = await dao.findAllSongs();
	res.send(result.rows);
})

router.get('/:id', (req: Request, res: Response) => {
    const id = Number(req.params.id);

    const getSongById = songs.find(song => song.id === id);

    if (getSongById) {
        res.send(getSongById);
    } else {
        res.status(404).json({ message: "Song not found" });
    }
})

router.post('/', async (req, res) => {
    const song: Song = req.body;
    await dao.insertSong(song)

    res.send({ message: "New song created succesfully." })
})

export default router;