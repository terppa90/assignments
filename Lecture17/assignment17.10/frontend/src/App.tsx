import { Link, Outlet } from 'react-router-dom'
import { songs } from './songList'

const App = () => {
  return (
    <div className='App'>
        <nav>
            {songs.map(({ id, title }) => {
                return <Link key={id} to={`/songs/${id.toString()}`}>{title}<br /></Link>
            })}
        </nav>

        <div className='Song'>
            <Outlet />

        </div>
    </div>
)
}

export default App