import React from 'react'
import ReactDOM from 'react-dom/client'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import App from './App.tsx'
import SongInfo, { loader as songLoader } from './songInfo.tsx'
import ErrorPage from './ErrorPage.tsx'

const router = createBrowserRouter([
  {
    path: '/',
    element: <App />,
    children: [{
      path: '/songs/:id',
      element: <SongInfo />,
      loader: songLoader,
      errorElement: <ErrorPage />
    }],
    errorElement: <ErrorPage />
  }
])

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
)
