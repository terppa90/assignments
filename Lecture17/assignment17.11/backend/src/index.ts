import express, { Request, Response } from "express";
import { Song, songs } from "./songList";

const server = express()

// server.use('/', express.static('./dist/client'))

server.get('/songs', (req: Request, res: Response) => {
    const getAllSongs = songs.map(((song: Song) => (
        { "id": song.id, "title": song.title }
    )));
    res.send( getAllSongs );
})

server.get('/songs/:id', (req: Request, res: Response) => {
    const id = Number(req.params.id);

    const getSongById = songs.find(song => song.id === id);

    if (getSongById) {
        res.send(getSongById);
    } else {
        res.status(404).json({ message: "Song not found" });
    }
})

server.listen(3000, () => {
    console.log('Server listening port 3000')
})
