/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react'
import { CSSProperties } from 'react'

const style: CSSProperties = {
  backgroundColor: 'yellow',
  position: 'absolute',
  margin: 'auto',
  width: '100px',
  height: '100px',
  display: 'block',
  alignContent: 'center',
  justifyContent: 'center',
  border: '2px solid black',
  cursor: 'pointer'
}

const App = () => {
  let offsetX: number, offsetY: number

  const move = (e: any) => {
    const el = e.target
    el.style.left = `${e.pageX - offsetX}px`
    el.style.top = `${e.pageY - offsetY}px`
  }

  const add = (e: any) => {
    const el = e.target
    offsetX = e.clientX - el.getBoundingClientRect().left
    offsetY = e.clientY - el.getBoundingClientRect().top
    el.addEventListener('mousemove', move)
  }

  const remove = (e: any) => {
    const el = e.target
    el.removeEventListener('mousemove', move)
  }

  return (
    <div style={style} onMouseDown={add} onMouseUp={remove}></div>
  )
}

export default App