import React from 'react'
import ReactDOM from 'react-dom/client'
// import App from './App.tsx'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import './index.css'

const Contacts = () => {
  return <div className='Page1'>
      Contacts Page
  </div>
}

const router = createBrowserRouter([
  {
    path: '/',
    element: <div>Main page</div>,
  },
  {
    path: '/contacts',
    element: <div><Contacts /></div>,
  }
])


ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
)
