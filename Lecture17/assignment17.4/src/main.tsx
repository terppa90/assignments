import React from 'react'
import ReactDOM from 'react-dom/client'
// import App from './App.tsx'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import { useLoaderData } from 'react-router-dom'
import './index.css'

interface Contacts {
  id: number,
  contactName: string,
  phoneNum: string,
  email: string,
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function loader({ params }: any) {
  return Number(params.id)
}

const Contacts = () => {
  const id = useLoaderData() as number
  const contacts: Contacts[] = [{ id: 1, contactName: "Contact 1", phoneNum: "0441234321", email: "contact1@gmail.com" }, { id: 2, contactName: "Contact 2", phoneNum: "0442134321", email: "contact2@gmail.com" }, { id: 3, contactName: "Contact 3", phoneNum: "0441334321", email: "contact3@gmail.com" }]

  const oneContact = contacts.find(contact => contact.id === id)

  // const singleContact = contacts.filter(contact => {
  //   return contact.id === id
  // })

  return <div className='Contacts'>
    {oneContact?.contactName}<br />
    {oneContact?.phoneNum}<br />
    {oneContact?.email}
    {/* Contacts Page {singleContact.map(contact => {
      return <div>{contact.contactName}<br />{contact.phoneNum}<br />{contact.email}</div>
    })} */}
  </div>
}

const router = createBrowserRouter([
  {
    path: '/',
    element: <div>Main page</div>,
  },
  {
    path: '/contacts/:id',
    element: <Contacts />,
    loader: loader
  }
])


ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
)
