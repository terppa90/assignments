import React from 'react'

export function loader({params}: any) {
    const id = Number(params.id)
    const contact = contacts.find(contact => contact.id === id)
    return contact
}

const Contact = ({contact}) => {
  return (
    <>
    <div>{contact.name}</div>
    <ul>
        <li>{contact.email}</li>
        <li>{contact.phone}</li>
    </ul>
    </>
  )
}

export default Contact