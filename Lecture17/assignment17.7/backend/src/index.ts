import express, { Request, Response } from "express";

const server = express()
server.use('/', express.static('./dist/client'))
server.get('/api/v1/version', (req: Request, res: Response) => {
    res.send("version 1.0")
}) 

server.get('*', (req, res) => {
    res.sendFile('index.html', { root: './dist/client'})
})

server.listen(3000, () => {
    console.log('Server listening port 3000')
})
