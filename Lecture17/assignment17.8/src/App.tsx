import React, { useState } from 'react'
import { CSSProperties } from 'react'
import useGenerateRandomColor from "./useGenerateRandomColor";



const App = () => {
  const { color, generateColor } =
    useGenerateRandomColor();

  const style: CSSProperties = {
    backgroundColor: "#" + color,
    margin: 'auto',
    width: '200px',
    height: '200px',
    display: 'block',
    alignContent: 'center',
    justifyContent: 'center',
    border: '2px solid black'
  }

  return (
    <>
    <div style={{margin: "auto", textAlign: "center"}}>#{color}</div>
    <div style={style} onClick={generateColor}>Click new color</div>
    </>
  )
}

export default App