import { useLoaderData } from 'react-router-dom'
import { Song, songs } from './songList'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function loader({ params }: { params: any}) {
    const id = Number(params.id)
    const song = songs.find(song => song.id === id)
    if (song === undefined) {
        throw new Response("Not Found", { status: 404 })
    }
    return song
}

export default function SongInfo() {
    const { title, lyrics, } = useLoaderData() as Song

    console.log(title, lyrics);
    

    return <div className='Song'>
        <h1>{title}</h1>
        <p>{lyrics}</p>
    </div>
}