// Add "type": "module" to package.json
import { pi, round } from "mathjs";
// Round pi with 10 decimals (3.1415926536)
const result = round(pi, 10);

console.log(result);
