const input = process.argv[2];

if (input === "fi") {
	console.log("Terve Maailma!");
} else if (input === "en") { // Tänhän ei tarvii olla täällä, koska se on defaultti
	console.log("Hello World!");
} else if (input === "es") {
	console.log("Hola Mundo!");
} else {
	console.log("Hello World!");
}

// Type npm start fi/en/es to terminal
