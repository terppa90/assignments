// convert a string into a number
const a: number = Number(process.argv[2])
const b: number = Number(process.argv[3])
const c: string = process.argv[4] + ' ' + process.argv[5]
// const c = process.argv[4] ja npm start 10 20 "hello world"

if (a > b) {
  console.log('a is greater')
} else if (b > a) {
  console.log('b is greater')
} else if (a === b && c === 'hello world') {
  console.log('yay, you guessed the password')
} else if (a === b) {
  console.log('they are equal')
}
