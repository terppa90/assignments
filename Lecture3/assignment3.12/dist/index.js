"use strict";
const number1 = Number(process.argv[2]);
const number2 = Number(process.argv[3]);
const number3 = Number(process.argv[4]);
const arr = [number1, number2, number3];
const sortedArr = arr.sort();
if (number1 === number2 && number2 === number3) {
    console.log("All numbers are equal.");
}
else {
    console.log(`Largest number: ${sortedArr[2]}\nSmallest number ${sortedArr[0]}`);
}
