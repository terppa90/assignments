const number1: number = Number(process.argv[2]);
const number2: number = Number(process.argv[3]);
const number3: number = Number(process.argv[4]);
// ^ eksplisiittiset numerotyypit on turhia kun se parsitaan numeroksi samalla rivillä

const arr = [number1, number2, number3];

const sortedArr = arr.sort();

if (number1 === number2 && number2 === number3) {
	console.log("All numbers are equal.");
} else {
	console.log(`Largest number: ${sortedArr[2]}\nSmallest number ${sortedArr[0]}`);
}
