const number: number = Number(process.argv[2]);

const numDays = (y: number, m: number) => new Date(y, m, 0).getDate();
console.log(numDays(2023, number));

// Month in JavaScript is 0-indexed (January is 0, February is 1, etc), 
// but by using 0 as the day it will give us the last day of the prior
// month. So passing in 1 as the month number will return the last day
// of January, not February