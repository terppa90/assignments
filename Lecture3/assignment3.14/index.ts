const balance: number = -5;
const isActive: boolean = true;
const checkBalance: boolean = true;

console.log("Check your balance?");

while (checkBalance) {
	if (isActive && balance > 0) {
		console.log(`Your balance is: ${balance}`);
		break;
	} else if (!isActive) {
		console.log("Your account is not active");
		break;
	} else if (isActive && balance === 0) { // tässä isActive tarkistus on turha, koska ollaan tultu !isActiven else-haaraan
		console.log("Your account is empty");
		break;
	} else {
		console.log("Your balance is negative");
		break;
	}
}
if (!checkBalance)
	console.log("Have a nice day");
