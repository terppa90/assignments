// Initial Letters

// const nameA: string = process.argv[2];
// const nameB: string = process.argv[3];
// const nameC: string = process.argv[4];

// const firstLetterA: string = nameA.charAt(0);
// const firstLetterB: string = nameB.charAt(0);
// const firstLetterC: string = nameC.charAt(0);

// console.log(`${firstLetterA}.${firstLetterB}.${firstLetterC}`);

// Length Comparison

const nameA: string = process.argv[2];
const nameB: string = process.argv[3];
const nameC: string = process.argv[4];

const arr: string[] = [nameA, nameB, nameC];

arr.sort(function(a, b){return b.length - a.length;});

console.log(arr);
