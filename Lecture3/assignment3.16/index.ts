const toLowerOrUpper: string = process.argv[2];
const input: string = process.argv[3];

if (toLowerOrUpper === "lower") {
	console.log(input.toLowerCase());
} else if (toLowerOrUpper === "upper") {
	console.log(input.toUpperCase());
}


