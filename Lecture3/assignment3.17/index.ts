let input: string = process.argv[2];

// Set the last index of empty space to variable lastIndex
const lastIndex = input.lastIndexOf(" ");

// Remove the word after last space " " using substring
input = input.substring(0, lastIndex);

console.log(input);


