"use strict";
const replacedChar = process.argv[2];
const charToReplace = process.argv[3];
const input = process.argv[4];
const regex = new RegExp(`${replacedChar}`, "g");
// 1. With replace method and regex
const result = input.replace(regex, charToReplace);
// 2. With replaceAll
// const result = input.replaceAll(replacedChar, charToReplace);
// 3. With split and join
const newStr = input.split(replacedChar).join(charToReplace);
console.log(result);
console.log(newStr);
// Hyvältä näyttää koodit! Lisää sun globaaliin gitignoreen myös dist-kansiot, niin ne ei myöskään lähde repoon mukaan.
