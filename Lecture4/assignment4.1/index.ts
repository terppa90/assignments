function wordsToUpperCase(words: string) {
	const arrOfWords = words.split(" ");
	let output = "";
    
	for (let i = 0; i < arrOfWords.length; i++) {
		const res = arrOfWords[i].charAt(0).toUpperCase() + arrOfWords[i].slice(1);

		output = output + res + " ";
	}

	return output;
    
}

console.log(wordsToUpperCase("jotain tekstiä"));

