// Find the first name in the list that is three letters long and ends in letter t.

const names = [
	"Murphy",
	"Hayden",
	"Parker",
	"Arden",
	"George",
	"Andie",
	"Ray",
	"Storm",
	"Tyler",
	"Pat",
	"Keegan",
	"Carroll"
];

// .slice(-1) could be also used to get the last char
const findWord = names.find((name) => {
	return name.length === 3 && name.charAt(name.length - 1) === "t";
});

console.log(findWord);





