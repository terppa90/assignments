// Create a function that takes two parameters: a list of strings and a separator 
// string. The function should join all the strings in the array, with the separator 
// string between each element, and then return the result.

const names = [
	"Murphy",
	"Hayden",
	"Parker",
	"Arden",
	"George",
	"Andie",
	"Ray",
	"Storm",
	"Tyler",
	"Pat",
	"Keegan",
	"Carroll"
];

function joiner(list: Array<string>, separator: string) {
	const joined = list.reduce((acc, cur) => {
		return acc + cur + separator;
	}, "");
	return joined.substring(0, joined.length - separator.length);
}

const joinedWords = joiner(names, ", ");
console.log(joinedWords);


