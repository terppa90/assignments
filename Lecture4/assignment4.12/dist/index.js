"use strict";
const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22];
// Create a new array with only the numbers that are divisible by three.
const divisibleByThree = arr.filter((num) => {
    if (num % 3 === 0) {
        return num;
    }
});
console.log(divisibleByThree);
// Create a new array from original array (arr), where each number is multiplied by 2
const multipliedByTwo = arr.map((num) => {
    return num * 2;
});
console.log(multipliedByTwo);
// Sum all of the values in the array using the array method reduce
const sumAllValues = arr.reduce((acc, cur) => {
    return acc + cur;
}, 0);
console.log(sumAllValues);
