"use strict";
// Create a program that takes in a number from the command line, 
// for example npm start 3 and prints a string "1 sheep...2 sheep...3 sheep..."
const input = Number(process.argv[2]);
let words = "";
const countSheep = (num) => {
    for (let i = 1; i <= num; i++) {
        words += i + " sheep...";
    }
    return words;
};
console.log(countSheep(input));
