// Create a program that turns any given word into charIndex version of the word

const charIndex = { a: 1, b: 2, c: 3, d: 4, e: 5, f: 6, g: 7, h: 8, i: 9, j: 10, k: 11, l: 12, m: 13, n: 14, o: 15, p: 16, q: 17, r: 18, s: 19, t: 20, u: 21, v: 22, w: 23, x: 24, y: 25, z: 26 };

const inputWord: string = process.argv[2];

function turnIntoCharIndex(word: string) {
	// Create a array of splitted characters of the word passed in
	const splitWord = word.split("");

	let charIndexWord = "";

	// Do something with each array element with .map() method
	// and loop over each object key(x) with for .. in loop to check if the characters match.
	// Then add the index number to charIndexWord variable.

	splitWord.map((char) => {
		for (const x in charIndex) {
			if (x === char) {
				charIndexWord += charIndex[x as keyof typeof charIndex];
			}
		}
	});

	return charIndexWord;
}

console.log(turnIntoCharIndex(inputWord));



