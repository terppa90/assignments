"use strict";
// Create program that outputs competitors placements with following way: 
// ['1st competitor was Julia', '2nd competitor was Mark', '3rd competitor was Spencer', '4th competitor was Ann', 
// '5th competitor was John', '6th competitor was Joe']
const competitors = ["Julia", "Mark", "Spencer", "Ann", "John", "Joe"];
const ordinals = ["st", "nd", "rd", "th"];
competitors.map((elem, index) => {
    if (index >= 3) {
        console.log(`${index + 1}${ordinals[3]} competitor was ${elem}`);
    }
    else {
        console.log(`${index + 1}${ordinals[index]} competitor was ${elem}`);
    }
});
