"use strict";
// Write a program that takes in any two numbers from the command line, start and end. 
// The program creates and prints an array filled with numbers from start to end.
const startNum = Number(process.argv[2]);
const endNum = Number(process.argv[3]);
// const arrayRange = (startNum: number, endNum: number, step: number) =>
// 	Array.from(
// 		{ length: (endNum - startNum) / step + 1 },
// 		(value, index) => startNum + index * step
// 	);
// console.log(arrayRange(startNum, endNum, 1));
const range = (min, max) => Array(max - min + 1).fill(0).map((_, i) => min + i);
console.log(range(startNum, endNum));
