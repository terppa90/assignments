"use strict";
// Create a programs that reverses each word in a string.
const inputString = process.argv[2];
function reverseWords(stringToReverse) {
    return stringToReverse.split("").reverse().join("").split(" ").reverse().join(" ");
}
console.log(reverseWords(inputString));
