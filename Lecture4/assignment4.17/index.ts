// Create a programs that reverses each word in a string.

const inputString: string = process.argv[2];

function reverseWords(stringToReverse: string){
	return stringToReverse.split("").reverse().join("").split(" ").reverse().join(" ");
}
    
console.log(reverseWords(inputString));