// Check if given string is a palindrome.

const input: string = process.argv[2];

function checkPalindrome(str: string) {
	// Use regex to remove unwanted spaces
	const regex = /[\W_]/g;
	// Lowercase the string so that it's case insensitive. The program ignores case and matches 
	// values regardless of their lower or upper case letters
	const strToLow = str.toLowerCase().replace(regex, "");

	// Turn the word backwards
	const reverseStr = strToLow.split("").reverse().join("");

	// Ternary condition to show the message
	return reverseStr === strToLow ? `Yes, ${str} is a palindrome` : `No, ${str} is not a palindrome`;
}

console.log(checkPalindrome(input));
