// Create a program that every time you run it, prints out an array 
// with differently randomized order of the array above.

const array: number[] = [2, 4, 5, 6, 8, 10, 14, 18, 25, 32];

const shuffled = array
	.map(value => ({ value, sort: Math.random() }))
	.sort((a, b) => a.sort - b.sort)
	.map(({ value }) => value);
   
console.log(shuffled);
