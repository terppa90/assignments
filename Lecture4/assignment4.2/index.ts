function generateRandNum(min: number, max: number): number{
	const randNum = Math.floor((Math.random() * (max - min)) + min);
	return randNum;
}

const res = generateRandNum(1, 10);
console.log(res);
