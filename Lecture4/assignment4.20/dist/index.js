"use strict";
// Fibonacci Sequence
const input = Number(process.argv[2]);
// Fibonacci with reduce
// function fibonacci(n: number){
// 	return new Array(n).fill(1).reduce((arr, _ ,i) => {
// 		arr.push((i <= 1) ? i : arr[i-2] + arr[i-1]);
// 		return arr;
// 	},[]) ;
// }
// console.log(fibonacci(input));
const fibonacci = function (num) {
    let num1 = 0;
    let num2 = 1;
    const arr = [];
    let temp;
    for (let i = 0; i < num; i++) {
        // console.log(num1);
        arr.push(num1);
        temp = num1 + num2;
        num1 = num2;
        num2 = temp;
    }
    return arr;
};
console.log(fibonacci(input));
