"use strict";
// Prime Number
const input = Number(process.argv[2]);
let isPrime = true;
const checkPrime = function (num) {
    if (num === 1 || num < 0) {
        return `${num} is not a prime number.`;
    }
    else if (num > 1) {
        for (let i = 2; i < num; i++) {
            if (num % i == 0) {
                isPrime = false;
                break;
            }
        }
        if (isPrime) {
            return `${num} is a prime number`;
        }
        else {
            return `${num} is not a prime number`;
        }
    }
};
console.log(checkPrime(input));
