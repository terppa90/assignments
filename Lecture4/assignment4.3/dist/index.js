"use strict";
function sumOfNums(num1, num2, num3 = 0) {
    const sum = num1 + num2 + num3;
    return sum;
}
console.log(sumOfNums(1, 2, 3));
// Anonymous Function
const anoSumOfNums = function (num1, num2, num3 = 0) {
    const sum = num1 + num2 + num3;
    return sum;
};
console.log(anoSumOfNums(1, 2, 3));
// Arrow Function
const arrowSumOfNums = (num1, num2, num3 = 0) => {
    const sum = num1 + num2 + num3;
    return sum;
};
console.log(arrowSumOfNums(1, 2));
