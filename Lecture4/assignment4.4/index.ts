// Math.random() always returns a number lower than 1.
// Use Math.floor() to return random integer and + 1 to get the correct range.
function rollDice(max: number) {
	const randRoll = Math.floor(Math.random() * max) + 1;

	return randRoll;
}

console.log(rollDice(6));
console.log(rollDice(8));
  

  


