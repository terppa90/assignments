"use strict";
// A function that calculates the sum of all the numbers that are smaller than a billion (1_000_000_000)
// and Divisible by 3, 5 and 7.
function calcSum(callback) {
    let sum = 0;
    for (let i = 1; i < 1000000000; i++) {
        if (i % 3 === 0 && i % 5 === 0 && i % 7 === 0) {
            sum += i;
        }
    }
    callback(sum);
}
// Log the result
const cb = (n) => console.log(n);
calcSum(cb);
