// A function that calculates the sum of all the numbers that are smaller than a billion (1_000_000_000)
// and Divisible by 3, 5 and 7.
function calcSum(callback: (result: number) => void) {
	let sum = 0;

	for (let i = 1; i < 1_000_000_000; i++) {
		if (i % 3 === 0 && i % 5 === 0 && i % 7 === 0) {
			sum += i;
		}	
	}

	callback(sum);
}

// Log the result
const cb = (n: number) => console.log(n);

calcSum(cb);
  

  


