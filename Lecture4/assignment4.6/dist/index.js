"use strict";
// Write a program that calculates the factorial n of a given number n using recursion.
const factorialize = function (num) {
    if (num === 0 || num === 1)
        return 1;
    for (let i = num - 1; i >= 1; i--) {
        num = num * i;
    }
    return num;
};
console.log(factorialize(3));
