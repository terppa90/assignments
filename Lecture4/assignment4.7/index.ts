const names = [
	"rauni",
	"matias",
	"Kimmo",
	"Heimo",
	"isko",
	"Sulevi",
	"Mikko",
	"daavid",
	"otso",
	"herkko"
];

names.forEach((name) => {
	console.log(name.charAt(0).toUpperCase() + name.slice(1));
});

