const words = [
	"city",
	"distribute",
	"battlefield",
	"relationship",
	"spread",
	"orchestra",
	"directory",
	"copy",
	"raise",
	"ice"
];

const reversedArr: string[] = [];

words.map(function(word) {
	const reversed = word.split("").reverse().join("");

	reversedArr.push(reversed);
});

console.log(reversedArr);
