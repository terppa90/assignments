// A program with function that takes a list of numbers and returns a list with all the items from the input list that are 
// divisible by three or five, but are not divisible by both.

const numbers = [
	749385,
	498654,
	234534,
	345467,
	956876,
	365457,
	235667,
	464534,
	346436,
	873453
];

// Filter creates new array
const filteredNums = numbers.filter((num) => {
	if (num % 5 === 0 && num % 3 === 0) {
		return false;
	} 

	if (num % 5 === 0 || num % 3 === 0) {
		return num;
	}

	return false;
});

console.log(filteredNums);





