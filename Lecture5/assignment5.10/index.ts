class Point {
	x: number
	y: number

	constructor(x: number, y: number) {
		this.x = x
		this.y = y
	}

    moveNorth(distance: number) {
        this.y += distance;
    }

    moveEast(distance: number) {
        this.x += distance;
    }

    moveSouth(distance: number) {
        this.y -= distance;
    }

    moveWest(distance: number) {
        this.x -= distance;
    }

    toString() {
        return `(${this.x}, ${this.y})`;
    }
}

const move = new Point(0, 0)

move.moveEast(10)
move.moveNorth(20)

console.log(move.toString())