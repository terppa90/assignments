"use strict";
class Point {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
    moveNorth(distance) {
        this.y += distance;
    }
    moveEast(distance) {
        this.x += distance;
    }
    moveSouth(distance) {
        this.y -= distance;
    }
    moveWest(distance) {
        this.x -= distance;
    }
    toString() {
        return `(${this.x}, ${this.y})`;
    }
}
const move = new Point(0, 0);
move.moveEast(10);
move.moveNorth(20);
console.log(move.toString());
