// Tämä näyttää olevan kesken

class Bank {
	name: string
	accounts: Array<Account>

	constructor(name: string, accounts: Array<Account>) {
		this.name = name
		this.accounts = accounts
	}

    openAccount(owner: string) {
        const id = "id" + Math.random().toString(16).slice(2)
        const acc = new Account(owner, id, 0)

        this.accounts.push(acc)

        return id
    }

    closeAccount(id: string) {
        
    }

    deposit(accountId: string, amount: number) {
        
    }

    withdraw(accountId: string, amount: number) {

    }

    checkBalace(accountId: string) {
        
    }

    transfer(fromAccountId: string, toAccountId: string, amount: number) {
        
    }

    customers() {
        
    }

    totalValue() {
        
    }
}

class Account {
    owner: string
    id: string
    balance: number

    constructor(owner: string, id: string, balance: number) {
		this.owner = owner
		this.id = id
        this.balance = balance
	}

    deposit(amount: number) {
        if (amount > 0) {
            this.balance += amount
        }
    }

    withdraw(amount: number) {
        
    }
}

// const account1 = new Bank()

// const move = new Point(0, 0)

// move.moveNorth(20)

// console.log(move.toString())