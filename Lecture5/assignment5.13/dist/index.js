"use strict";
class Post {
    constructor(author, content, likers) {
        this.author = author;
        this.content = content;
        this.likers = likers;
    }
    toString() {
        return `${this.author} ${this.content} ${this.likers}`;
    }
    like(user) {
        if (!this.likers.includes(user)) {
            this.likers.push(user);
        }
    }
    likes() {
        if (this.likers.length === 0) {
            return `No one likes this`;
        }
        if (this.likers.length === 1) {
            return `${this.likers[0]} likes this`;
        }
        if (this.likers.length === 2) {
            return `${this.likers[0]} and ${this.likers[1]} likes this`;
        }
        if (this.likers.length === 3) {
            return `${this.likers[0]}, ${this.likers[1]} and ${this.likers[2]} likes this`;
        }
        if (this.likers.length === 4) {
            return `${this.likers[0]}, ${this.likers[1]} and ${this.likers.length - 2} others likes this`;
        }
        if (this.likers.length >= 5) {
            return `${this.likers[0]}, ${this.likers[1]} and ${this.likers.length - 2} others likes this`;
        }
    }
}
const post1 = new Post("Tero", "Eka postaus", []);
post1.like("Tero");
post1.like("Jussi");
post1.like("Joni");
post1.like("Irmeli");
post1.like("Anna");
post1.likes();
console.log(post1.toString());
console.log(post1.likes());
