"use strict";
function generateUsername(firstname, lastname) {
    // Username
    const currentYear = new Date().getFullYear();
    const twoNums = currentYear.toString().substring(2);
    const firstLetters1 = firstname.substring(0, 2).toLowerCase();
    const firstLetters2 = lastname.substring(0, 2).toLowerCase();
    const username = `B${twoNums}${firstLetters1}${firstLetters2}`;
    return username;
}
function generatePassword(firstname, lastname) {
    // Password
    const currentYear = new Date().getFullYear();
    const twoNums = currentYear.toString().substring(2);
    const firstLetter = firstname.substring(0, 1).toLowerCase();
    const lastLetter = lastname.slice(-1).toUpperCase();
    const randNumForLetter = Math.floor(Math.random() * 90) + 65;
    const randLetter = String.fromCharCode(randNumForLetter);
    const randNumForSpecialChar = Math.floor(Math.random() * 47) + 33;
    const randSpecialChar = String.fromCharCode(randNumForSpecialChar);
    const password = `${randLetter}${firstLetter}${lastLetter}${randSpecialChar}${twoNums}`;
    return password;
}
console.log(generateUsername("Erkki", "Esimerkki"));
console.log(generatePassword("Erkki", "Esimerkki"));
