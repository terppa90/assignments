"use strict";
const n = 3;
function numberOfSteps(num) {
    let count = 0;
    while (num !== 1) {
        if (num % 2 === 0) {
            num = num / 2;
            count++;
        }
        else if (num % 2 !== 0) {
            num = (num * 3) + 1;
            count++;
        }
    }
    return `${count} steps`;
}
console.log(numberOfSteps(n));
