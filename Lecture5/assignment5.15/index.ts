const n: number = 3;

function numberOfSteps(num: number) {
	let count: number = 0;

	while (num !== 1) {
		if (num % 2 === 0) {
			num = num / 2;
			count++;
		}
	
		else if (num % 2 !== 0) {
			num = (num * 3) + 1;
			count++;
		}
        // koska tarkastellaan kahdella jaollisuutta, mahdollisuudet ovat että se joko on tai ei ole jaollinen, joten mielummin
        // if (num % 2 === 0) {
        //     ...
        // } else {
        //     ...
        // }
        // lisäksi count++ kannattaa siirtää if-lauseen ulkopuolelle, koska se tehdään joka tapauksessa
	}
	return `${count} steps`;

}


console.log(numberOfSteps(n));


