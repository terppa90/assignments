"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
const readline_1 = __importDefault(require("readline"));
// Read a file
const studentAnswers = fs_1.default.createReadStream("./student_answers.txt", "utf-8");
const correctAnswers = fs_1.default.createReadStream("./correct_answers.txt", "utf-8");
// write a file
const writeWords = fs_1.default.createWriteStream("./resultText.txt", "utf-8");
// studentAnswers.on("data", txt => {
// 	const modifiedText = txt.toString();
// 	console.log(modifiedText);
// 	writeWords.write(modifiedText, error => {
// 		if (error) console.error(error);
// 	});
// });
// Tässä tuli jotain yritettyä :D
function examScores(studentAnswers, correctAnswers) {
    correctAnswers.on("data", txt => {
        const answerText = txt.toString();
        const splitCorrect = answerText.split(",");
        const rl = readline_1.default.createInterface({
            input: studentAnswers,
            crlfDelay: Infinity
        });
        rl.on("line", function (line) {
            let points = 0;
            const splitted = line.split(",");
            for (let i = 0; i < splitted.length; i++) {
                if (splitted[i] === splitCorrect[i]) {
                    points += 4;
                }
                else if (splitted[i] !== splitCorrect[i]) {
                    points -= 1;
                }
                else if (splitted[i] === "") {
                    points += 0;
                }
            }
            // writeWords.write(points, error => {
            // 	if (error) console.error(error);
            // });
            if (points > 0)
                console.log(points);
            else
                console.log(0);
        });
        rl.on("close", function () {
            console.log("Finished");
        });
    });
}
console.log(examScores(studentAnswers, correctAnswers));
