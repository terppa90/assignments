import fs from "fs";
import readline from "readline";

// Read a file
const studentAnswers = fs.createReadStream("./student_answers.txt", "utf-8");
const correctAnswers = fs.createReadStream("./correct_answers.txt", "utf-8");
// write a file
// const writeWords = fs.createWriteStream("./resultText.txt", "utf-8");

// studentAnswers.on("data", txt => {
// 	const modifiedText = txt.toString();
// 	console.log(modifiedText);
// 	writeWords.write(modifiedText, error => {
// 		if (error) console.error(error);
// 	});
// });

// Tässä tuli jotain yritettyä :D
function examScores(studentAnswers: fs.ReadStream, correctAnswers: fs.ReadStream) {
	correctAnswers.on("data", txt => {
		const answerText = txt.toString();
		const splitCorrect = answerText.split(",");

		const rl = readline.createInterface({
			input: studentAnswers,
			crlfDelay: Infinity
		});
	
		rl.on("line", function(line) {
			let points = 0;
			const splitted = line.split(",");
			for (let i = 0; i < splitted.length; i++) {
				if (splitted[i] === splitCorrect[i]) {
					points += 4;
				} else if (splitted[i] !== splitCorrect[i]) {
					points -= 1;
				} else if (splitted[i] === "") {
					points += 0;
				}
			}

			// writeWords.write(points, error => {
			// 	if (error) console.error(error);
			// });

			if (points > 0)
				console.log(points);
			else
				console.log(0);
		});
		
		rl.on("close", function() {
			console.log("Finished");
		});

	});

}

console.log(examScores(studentAnswers, correctAnswers));

// Tässä kaipaisin nyt hieman selventäviä funktioita. Ohjelmassahan siis 
// - luetaan kaksi tiedostoa
// - pilkotaan tiedostoista tullut tavara riveiksi
// - pilkotaan rivit sarakkeiksi
// - käydään sarakkeet läpi yksi kerrallaan verraten niitä toisiinsa
// - pisteytetään tulosten mukaan
// - kirjoitetaan tulokset tiedostoon

// Näistä olisi hyvä tehdä omia funktioitaan, jotta luettaessa olisi heti selvää, mitä missäkin kohdassa tapahtuu. Nyt erityisesti streameja käyttäessä meillä on kaksi sisäkkäistä rakennetta, joista jälkimmäisen sisällä on vielä vertailu- ja pisteutuslogiikkaa.

// Funktio kannattaa myös kirjoittaa lähes aina muotoon, jossa se palauttaa tuloksen, ja hoitaa loggaaminen sitten funktion itsensä ulkopuolella. Näin funktiolla on vähemmän sivuvaikutuksia ja se on erikoistuneempi hoitamaan yhtä asiaa.

// Kirjoitusosuus näyttää puuttuvan vielä