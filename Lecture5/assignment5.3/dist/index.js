"use strict";
const firstIngredient = {
    name: "eggs",
    amount: 3
};
const secondIngredient = {
    name: "sugar",
    amount: 2
};
const firstRecipe = {
    name: "Cake",
    ingredients: [firstIngredient, secondIngredient],
    serving: 2
};
console.log(firstRecipe);
