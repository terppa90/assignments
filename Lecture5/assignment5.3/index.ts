interface Ingredient {
	name: string,
	amount: number
}

interface Recipe {
	name: string,
	ingredients: Array<Ingredient>,
	serving: number
}

const firstIngredient: Ingredient = {
	name: "eggs",
	amount: 3
};

const secondIngredient: Ingredient = {
	name: "sugar",
	amount: 2
};

const firstRecipe: Recipe = {
	name: "Cake",
	ingredients: [firstIngredient, secondIngredient],
	serving: 2
};

console.log(firstRecipe);