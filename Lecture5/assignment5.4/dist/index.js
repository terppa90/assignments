"use strict";
const firstIngredient = {
    name: "eggs",
    amount: 3
};
const secondIngredient = {
    name: "sugar",
    amount: 2
};
const firstRecipe = {
    name: "Cake",
    ingredients: [firstIngredient, secondIngredient],
    serving: 1,
    setServings: setServingsFunc
    // setServings: function(servings: number) {
    // 	this.serving = servings;
    // 	for (let i = 0; i < this.ingredients.length; i++) {
    // 		this.ingredients[i].amount = this.ingredients[i].amount * this.serving
    // 	}
    // }
};
function setServingsFunc(servings) {
    firstRecipe.serving = servings;
    for (let i = 0; i < firstRecipe.ingredients.length; i++) {
        firstRecipe.ingredients[i].amount = firstRecipe.ingredients[i].amount * firstRecipe.serving;
    }
}
console.log(firstRecipe);
firstRecipe.setServings(10);
console.log(firstRecipe);
