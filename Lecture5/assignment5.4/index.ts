interface Ingredient {
	name: string,
	amount: number
}

interface Recipe {
	name: string,
	ingredients: Array<Ingredient>,
	serving: number,
	setServings: (servings: number) => void
}

const firstIngredient: Ingredient = {
	name: "eggs",
	amount: 3
};

const secondIngredient: Ingredient = {
	name: "sugar",
	amount: 2
};

const firstRecipe: Recipe = {
	name: "Cake",
	ingredients: [firstIngredient, secondIngredient],
	serving: 1,
	// setServings: setServingsFunc
	
	// setServings: function(servings: number) {
	// 	this.serving = servings;

	// 	for (let i = 0; i < this.ingredients.length; i++) {
	// 		this.ingredients[i].amount = this.ingredients[i].amount * this.serving
	// 	}
	// }

	// Esimerkkiratkaisu
	setServings: function(servings: number) {
        const factor = servings / this.serving 
        this.ingredients = this.ingredients.map((ingredient: Ingredient) => {
            return {...ingredient, amount: factor * ingredient.amount }
        })
        this.serving = servings
    }
};

console.log(firstRecipe);

firstRecipe.setServings(10);

console.log(firstRecipe);