"use strict";
class Ingredient {
    constructor(name, amount) {
        this.name = name;
        this.amount = amount;
    }
}
class Recipe {
    constructor(name, ingredients, serving) {
        this.name = name;
        this.ingredients = ingredients;
        this.serving = serving;
    }
    setServings(servings) {
        const factor = servings / this.serving;
        this.ingredients = this.ingredients.map(ingredient => {
            return Object.assign(Object.assign({}, ingredient), { amount: factor * ingredient.amount });
        });
        this.serving = servings;
        this.serving = servings;
    }
    toString() {
        return this.ingredients.reduce((acc, cur) => {
            return acc + `- ${cur.name} (${cur.amount})\n`;
        }, `${this.name} (${this.serving} servings)\n\n`);
    }
}
const flour = new Ingredient('flour', 300);
const water = new Ingredient('water', 150);
const oil = new Ingredient('Oil', 30);
const salt = new Ingredient('Salt', 0);
const tortillas = new Recipe('tortillas', [flour, water, oil, salt], 12);
tortillas.setServings(24);
console.log(tortillas.toString());
