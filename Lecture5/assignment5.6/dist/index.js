"use strict";
class Ingredient {
    constructor(name, amount) {
        this.name = name;
        this.amount = amount;
    }
}
class Recipe {
    constructor(name, ingredients, serving) {
        this.name = name;
        this.ingredients = ingredients;
        this.serving = serving;
    }
    setServings(servings) {
        const factor = servings / this.serving;
        this.ingredients = this.ingredients.map(ingredient => {
            return Object.assign(Object.assign({}, ingredient), { amount: factor * ingredient.amount });
        });
        this.serving = servings;
        this.serving = servings;
    }
    toString() {
        return this.ingredients.reduce((acc, cur) => {
            return acc + `- ${cur.name} (${cur.amount})\n`;
        }, `${this.name} (${this.serving} servings)\n\n`);
    }
}
class HotRecipe extends Recipe {
    constructor(name, ingredients, serving, heatLevel) {
        super(name, ingredients, serving);
        this.heatLevel = heatLevel;
    }
    toString() {
        let recipeStr = super.toString();
        if (this.heatLevel > 5) {
            recipeStr += '\n' + `WARNING: This is a heat level ${this.heatLevel} recipe!`;
        }
        return recipeStr;
    }
}
const tomato = new Ingredient('tomato', 400);
const onion = new Ingredient('onion', 1);
const garlic = new Ingredient('garlic', 3);
const someChili = new Ingredient('chili', 1);
const lotsChili = new Ingredient('chili', 10);
const mildSalsa = new HotRecipe('Mild Salsa', [tomato], 6, 3);
mildSalsa.setServings(100);
const hotSalsa = new HotRecipe('Hot Salsa', [tomato, onion, garlic, lotsChili], 6, 8);
hotSalsa.setServings(100);
console.log(mildSalsa.toString());
console.log(hotSalsa.toString());
