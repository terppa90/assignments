import fs from "fs";

// Reading a file
const readWords = fs.createReadStream("./textfile.txt", "utf-8");
// writing a file
const writeWords = fs.createWriteStream("./resultText.txt", "utf-8");

readWords.on("data", txt => {
	const modifiedText = txt.toString().replaceAll("joulu", "kinkku")
		.replaceAll("Joulu", "Kinkku")
		.replaceAll("lapsilla", "poroilla")
		.replaceAll("Lapsilla", "Poroilla");
	console.log(modifiedText);
	writeWords.write(modifiedText, error => {
		if (error) console.error(error);
	});
});



console.log(newWords);
