console.log("3");
setTimeout(() => {
	console.log("2");
	setTimeout(() => {
		console.log("1");
		setTimeout(() => {
			console.log("GO!");
		}, 1000);
	}, 1000);
}, 1000);

// With callback
// const countDown = function (arg: number, func: { (): void; (): void; }) {
// 	setTimeout(() => {
// 		func();
// 	}, arg);
// };

// countDown(1000, () => {
// 	console.log("3");
// 	countDown(1000, () => {
// 		console.log("2");
// 		countDown(1000, () => {
// 			console.log("1");
// 			countDown(1000, () => {
// 				console.log("GO!");
// 			});
// 		});
// 	});
// });