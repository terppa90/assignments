/* eslint-disable indent */
console.log("3");

new Promise<void>((resolve, reject) => {
	setTimeout(() => {
		console.log("2");
		resolve();
	}, 1000);
})
.then(() => {
	return new Promise<void>((resolve, reject) => {
		setTimeout(() => {
			console.log("1");
			resolve();
		}, 1000);
	});
	
}).then(() => {
	setTimeout(() => {
        console.log("GO");
    }, 1000);
});