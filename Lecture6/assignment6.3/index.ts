const getValue = function(): Promise<number> {
	return new Promise((resolve, reject) => {
		setTimeout(() => {
			resolve(Math.random());
		}, Math.random() * 1500);
	});
};

const randValues = async () => {
	const val1 = await getValue();
	const val2 = await getValue();

	console.log(val1);
	console.log(val2);
};


const randValues2 = () => {
	let val1: number;

	getValue().then((val) => {
		val1 = val;
		return getValue();
	}).then((val2) => {
		console.log(val2, val1);
		
	});
};

console.log("start");

randValues();
randValues2();

console.log("end");

// const valuePromises = [
// 	getValue(),
// 	getValue()
// ];

// async function printAllValues() {
// 	const res = await Promise.all(valuePromises);
// 	console.log(res);
// }

// printAllValues();
 