import axios from "axios";

interface Movie {
	Title: string,
	Year?: number,
	imdbID: string,
	Type: string
}

const movieSearch = async (title: string, year?: number) => {
	const response = await axios.get("http://www.omdbapi.com/?apikey=c3a0092f&s=" + title + "&y=" + year);
	const typedResult: Movie[] = response.data.Search;

	console.log(typedResult);
	// console.log(typedResult[0].Year);
};

movieSearch("oppenheimer", 2023);