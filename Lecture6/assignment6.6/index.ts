const numbers = [3, 1, 5, 4, 10];
// let max = numbers[0];

const largestNum = function (numbers: number[]) {
	// for (let i = 1; i < numbers.length; i++) {
	// 	if (numbers[i] > max) {
	// 		max = numbers[i];
	// 	}
	// }

	// console.log(max);

	return numbers.reduce(function (a, b) {
		return (a > b) ? a : b;
	});
    // paljon parempi ratkaisu kuin kommentoitu
};

const secondLargestNum = function (numbers: number[]) {
	let largest = numbers[0];
	let secondLargest = -Infinity;

	for (let i = 1; i < numbers.length; i++) {
		if (numbers[i] > largest) {
			secondLargest = largest;
			largest = numbers[i];
		} else if (numbers[i] < largest && numbers[i] > secondLargest) {
			secondLargest = numbers[i];
		}
	}
	return secondLargest;

};

console.log(largestNum(numbers));
console.log(secondLargestNum(numbers));