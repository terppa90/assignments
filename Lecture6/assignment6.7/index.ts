const students = [
	{ name: "Markku", score: 99 },
	{ name: "Karoliina", score: 58 },
	{ name: "Susanna", score: 69 },
	{ name: "Benjamin", score: 77 },
	{ name: "Isak", score: 49 },
	{ name: "Liisa", score: 89 },
];

// Highest scoring student
const highestScoringStudent = students.reduce(function (prev, current) {
	return (prev && prev.score > current.score) ? prev : current;
});
// jos et anna alkuarvoa, prev arvo on ensimmäisellä kierroksella ensimmäinen arrayn arvo, joten tässä toi eka tarkistus (prev &&) on tarpeeton

// Lowest scoring student
const lowestScoringStudent = students.reduce(function (prev, current) {
	return (prev && prev.score < current.score) ? prev : current;
});

// Average
const average = students.reduce((total, next) => total + next.score, 0) / students.length;

// hyvää reducen käyttöä

// Print higher than avg
const getGreaterThanAverage = function (students: {name: string, score: number}[]) {
	const studentsAboveAvg: string[] = [];

	for (let i = 0; i < students.length; i++) {
		if (students[i].score > average) {
			studentsAboveAvg.push(students[i].name);
		}
	}
    // tässä .filter olisi toiminut
    // const aboveAvgStudents = students.filter(student => student.score > average)

	return studentsAboveAvg;
};

const studentGrades = (students: {
	grade?: number;name: string, score: number
}[]) => {
	students.map((student) => {
		if (student.score > 1 && student.score <= 39) {
			student.grade = 1;
		} else if (student.score > 39 && student.score <= 59) {
			student.grade = 2;
		} else if (student.score > 59 && student.score <= 79) {
			student.grade = 3;
		} else if (student.score > 79 && student.score <= 94) {
			student.grade = 4;
		} else if (student.score > 94 && student.score <= 100) {
			student.grade = 5;
		}
	});
};

console.log(highestScoringStudent);
console.log(lowestScoringStudent);
console.log(average);
console.log("Students with higher than avg scores: " + getGreaterThanAverage(students));
console.log(studentGrades(students));
console.log(students);

