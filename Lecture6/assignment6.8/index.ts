import axios from "axios";

interface User {
	name: string,
	username: string,
	email: string,
}

// Console log out data
// axios.get("https://jsonplaceholder.typicode.com/todos/")
// 	.then(response => console.log(response.data))
// 	.catch(error => console.error(error));

// ^tää on ihan oikein oleva ykköskohta, miks se on kommentoitu pois?

// Get all users
// const getUsers = async () => {
// 	const response = await axios.get("https://jsonplaceholder.typicode.com/users/");
// 	const typedResult: User = response.data;

// 	console.log(typedResult);
// };

// getUsers();

// Get one user by id and show only name, username and email
// const getOneUser = async (userId: number) => {
// 	const response = await axios.get(`https://jsonplaceholder.typicode.com/users/${userId}`);
// 	const typedResult: User = response.data;

// 	console.log(typedResult.name);
// 	console.log(typedResult.username);
// 	console.log(typedResult.email);
// };

// getOneUser(3);



// const modifyData = async (userId: number) => {
// 	const user = await axios.get(`https://jsonplaceholder.typicode.com/users/${userId}`);
// 	const typedResult: User = user.data;

// 	console.log(typedResult);

// 	const todos = await axios.get("https://jsonplaceholder.typicode.com/todos/");
// };

// modifyData(3);


// Jotain tuli yritettyä
const request1 = fetch("https://jsonplaceholder.typicode.com/users/3").then(response => response.json());
const request2 = fetch("https://jsonplaceholder.typicode.com/todos/").then(response => response.json());
Promise.all([request1, request2])
	.then(([data1, data2]) => {
		data2.map((x: {userId: number, id: number}) => {
			x.userId = data1.id;
			console.log(data2);
		});
	})
	.catch(error => {
		console.error(error);
	});

// Ehdotan, että teet tän uudestaan, joko mallivastauksen kanssa tai voidaan katsoa sitä yhdessä. Toi tehtävänanto on melko epäselvä, mikä ei helpota tilannetta ollenkaan, mutta mä voin avata sitä lisää tarvittaessa. 