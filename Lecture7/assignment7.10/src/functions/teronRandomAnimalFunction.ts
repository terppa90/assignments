import { app, HttpRequest, HttpResponseInit, InvocationContext } from "@azure/functions";
import axios from "axios";

export async function teronRandomAnimalFunction(request: HttpRequest, context: InvocationContext): Promise<HttpResponseInit> {
    context.log(`Http function processed request for url "${request.url}"`);

    const catUrl = 'https://cataas.com/cat?json=true';
    const dogUrl = 'https://dog.ceo/api/breeds/image/random';

    fetch(catUrl)
        .then((response) => response.json())
        .then((data) => {
            console.log(data);
        });

        fetch(dogUrl)
        .then((response) => response.json())
        .then((data) => {
            console.log(data);
        });

    return { body: `Random cat picture: ` }
};

app.http('teronRandomAnimalFunction', {
    methods: ['GET', 'POST'],
    authLevel: 'anonymous',
    handler: teronRandomAnimalFunction
});

// Tämä lienee kesken? Kettuja ei ole ollenkaan, ja bodyyn menee nyt aina teksti 'Random cat picture: '