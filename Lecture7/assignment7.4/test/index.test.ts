import { calculator } from "..";

// test("dummy test", () => {
// 	expect(true).toBe(true);
// });

test("Multiplication returns 6 with parameters 2 and 3", () => {
	const res = calculator("*", 2, 3);
	expect(res).toBe(6);
});

test("Multiplication returns 4500 with parameters 150 and 30", () => {
	const res = calculator("*", 150, 30);
	expect(res).toBe(4500);
});

test("Multiplication returns 5 with parameters 10 and 0.5", () => {
	const res = calculator("*", 10, 0.5);
	expect(res).toBe(5);
});

test("Multiplication returns -5 with parameters -10 and 0.5", () => {
	const res = calculator("*", -10, 0.5);
	expect(res).toBe(-5);
});

describe("Division", () => {
	it("Returns 2 with parameters 10 and 5", () => {
		const res = calculator("/", 10, 5);
		expect(res).toBe(2);
	});

	it("Returns -2 with parameters 10 and -5", () => {
		const res = calculator("/", 10, -5);
		expect(res).toBe(-2);
	});

	it("Returns 20 with parameters 10 and 0.5", () => {
		const res = calculator("/", 10, 0.5);
		expect(res).toBe(20);
	});

	it("Returns 100 with parameters 10e7 and 1000000", () => {
		const res = calculator("/", 10e7, 1000000);
		expect(res).toBe(100);
	});
});

describe("Sum", () => {
	it("Returns 15 with parameters 10 and 5", () => {
		const res = calculator("+", 10, 5);
		expect(res).toBe(15);
	});

	it("Returns 5 with parameters 10 and -5", () => {
		const res = calculator("+", 10, -5);
		expect(res).toBe(5);
	});

	it("Returns 10.5 with parameters 10 and 0.5", () => {
		const res = calculator("+", 10, 0.5);
		expect(res).toBe(10.5);
	});
});

describe("Subtraction", () => {
	it("Returns 5 with parameters 10 and 5", () => {
		const res = calculator("-", 10, 5);
		expect(res).toBe(5);
	});

	it("Returns 15 with parameters 10 and -5", () => {
		const res = calculator("-", 10, -5);
		expect(res).toBe(15);
	});

	it("Returns 9.5 with parameters 10 and 0.5", () => {
		const res = calculator("-", 10, 0.5);
		expect(res).toBe(9.5);
	});
});