export function unitConverter(amount: number, sourceUnit: string, targetUnit: string): number {
	if (sourceUnit === "dl") {
		if (targetUnit === "l") {
			return Math.round(amount / 10);
		} else if (targetUnit === "oz") {
			return Math.round(amount * 3.3814);
		} else if (targetUnit === "cup") {
			return Math.round(amount * 0.422675);
		} else if (targetUnit === "pint") {
			return Math.round(amount * 0.2113376);
		}
	}

	if (sourceUnit === "l") {
		if (targetUnit === "dl") {
			return Math.round(amount * 10);
		} else if (targetUnit === "oz") {
			return Math.round(amount * 33.814);
		} else if (targetUnit === "cup") {
			return Math.round(amount * 4.22675);
		} else if (targetUnit === "pint") {
			return Math.round(amount * 2.113376);
		}
	}

	if (sourceUnit === "oz") {
		if (targetUnit === "dl") {
			return Math.round(amount * 0.295735);
		} else if (targetUnit === "l") {
			return Math.round(amount * 0.0295735);
		} else if (targetUnit === "cup") {
			return Math.round(amount * 0.125);
		} else if (targetUnit === "pint") {
			return Math.round(amount * 0.0625);
		}
	}

	if (sourceUnit === "cup") {
		if (targetUnit === "dl") {
			return Math.round(amount * 2.365882);
		} else if (targetUnit === "oz") {
			return Math.round(amount * 8);
		} else if (targetUnit === "l") {
			return Math.round(amount * 23.65882);
		} else if (targetUnit === "pint") {
			return Math.round(amount * 0.5);
		}
	}

	if (sourceUnit === "pint") {
		if (targetUnit === "dl") {
			return Math.round(amount * 4.73176);
		} else if (targetUnit === "oz") {
			return Math.round(amount * 16);
		} else if (targetUnit === "cup") {
			return Math.round(amount * 2);
		} else if (targetUnit === "l") {
			return Math.round(amount * 0.473176);
		}
	}

	return 1;
}

// aika paljon koodia, joka toistaa itseään. voisiko tämä tehdä lyhyemmin? yhden yksikön lisäämisessä on nyt aika iso homma, tai suhdevakion päivittämisessä

const amount = Number(process.argv[2]);
const sourceUnit = process.argv[3];
const targetUnit = process.argv[4];

console.log(unitConverter(amount, sourceUnit, targetUnit));
