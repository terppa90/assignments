import { unitConverter } from "..";

test("Convert 10 dl to l should return 1", () => {
	const res = unitConverter(10, "dl", "l");
	expect(res).toBe(1);
});

test("Convert 2 litres to ounces should return 68", () => {
	const res = unitConverter(2, "l", "oz");
	expect(res).toBe(68);
});

describe("Ounces", () => {
	it("Returns 1 with parameters 10, oz and cup", () => {
		const res = unitConverter(10, "oz", "cup");
		expect(res).toBe(1);
	});

	it("Returns 6 with parameters 20, oz and dl", () => {
		const res = unitConverter(20, "oz", "dl");
		expect(res).toBe(6);
	});
});

describe("Cup", () => {
	it("Returns 3 with parameters 5, cup and pint", () => {
		const res = unitConverter(5, "cup", "pint");
		expect(res).toBe(3);
	});

	it("Returns 24 with parameters 3, cup and oz", () => {
		const res = unitConverter(3, "cup", "oz");
		expect(res).toBe(24);
	});
});

describe("Pint", () => {
	it("Returns 8 with parameters 4, pint and cup", () => {
		const res = unitConverter(4, "pint", "cup");
		expect(res).toBe(8);
	});

	it("Returns 7 with parameters 15, pint and l", () => {
		const res = unitConverter(15, "pint", "l");
		expect(res).toBe(7);
	});
});