import { app, HttpRequest, HttpResponseInit, InvocationContext } from "@azure/functions";

export async function teronRandNumFunction(request: HttpRequest, context: InvocationContext): Promise<HttpResponseInit> {
    context.log(`Http function processed request for url "${request.url}"`);

    const minNum = Number(request.query.get('input1'));
    const maxNum = Number(request.query.get('input2'));
    const returnInt: boolean = JSON.parse(request.query.get('returnInt'));

    if (returnInt === true) {
        const randNum = Math.floor(Math.random() * (maxNum - minNum)) + minNum;
        return { body: `Random integer number between ${minNum} and ${maxNum}: ${randNum}` };
    } else if (returnInt === false) {
        const randNum = Math.random() * (maxNum - minNum) + minNum;
        return { body: `Random decimal number between ${minNum} and ${maxNum}: ${randNum}` };
    } 
    
};

app.http('teronRandNumFunction', {
    methods: ['GET', 'POST'],
    authLevel: 'anonymous',
    handler: teronRandNumFunction
});

// Random integer num between 1 and 10(excluded)
// http://localhost:7071/api/teronRandNumFunction?input1=1&input2=10&returnInt=true

// Random decimal num between 1 and 10(excluded)
// http://localhost:7071/api/teronRandNumFunaction?input1=1&input2=10&returnInt=false
