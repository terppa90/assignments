import { calculator } from "..";

test("dummy test", () => {
	expect(true).toBe(true);
});

test("Multiplication returns 6 with parameters 2 and 3", () => {
	const res = calculator("*", 2, 3);
	expect(res).toBe(6);
});

test("Multiplication returns 4500 with parameters 150 and 30", () => {
	const res = calculator("*", 150, 30);
	expect(res).toBe(4500);
});

test("Multiplication returns 5 with parameters 10 and 0.5", () => {
	const res = calculator("*", 10, 0.5);
	expect(res).toBe(5);
});

test("Multiplication returns -5 with parameters -10 and 0.5", () => {
	const res = calculator("*", -10, 0.5);
	expect(res).toBe(-5);
});