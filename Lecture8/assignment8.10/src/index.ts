import express, { Request, Response } from "express";

// Import middlewares
import { unknownEndpoint } from "./middlewares";
import { validate } from "./middlewares";
// import { validatePut } from "./middlewares";

const server = express();
server.use(express.json());

interface Student {
	id: string,
	name: string,
	score: string
}

let students: Student[] = [];

server.post("/students", validate, (req: Request, res: Response) => {
	const student: Student[] = req.body;

	students = student;

	res.send(student);
});

// Get all students
server.get("/students", (req: Request, res: Response) => {
	
	const getAllStudents = students.map((student => ({
		...student
	})));
	res.send(getAllStudents);
});

server.get("/scores", (req: Request, res: Response) => {
	let avg: number = 0;
	
	const scores = students.map(student => { 
		const score = Number(student.score); 
		return score;
	});

	const sumOfScores = scores.reduce(function(a, b) { return a + b; }, 0);

	avg = sumOfScores / scores.length;

	// Highest scoring student
	const highestScore = students.reduce(function(prev, current) {
		return (prev && Number(prev.score) > Number(current.score)) ? prev : current;
	});

	// Percentage of students above or equal to avg score
	let average: number = 0;

	const sumScores = scores.reduce(function(a, b) { return a + b; }, 0);

	average = sumScores / scores.length;

	const higerOrEqualThanAvg: string[] = [];

	students.map(student => { 
		const score = Number(student.score); 
		if (score >= average) {
			higerOrEqualThanAvg.push(score.toString());
		}
		return higerOrEqualThanAvg;
	});

	const percentage = (higerOrEqualThanAvg.length / students.length) * 100;
	
	res.json({"Average student scores": avg.toString(), "Highest scoring student":  highestScore.name, "Percentage of students with higher or equal than average score": percentage + "%"});
});

// Get student by id
server.get("/students/:id", (req: Request, res: Response) => {
	const id = req.params.id;

	const getStudentById = students.find(student => student.id === id);

	if (getStudentById) {
		res.send(getStudentById);
	} else {
		res.status(404).json({ message: "User not found" });
	}
});

// Delete a student by id
server.delete("/students/:id", (req: Request, res: Response) => {
	const id = req.params.id;

	const index = students.findIndex(student => student.id === id);

	if (index !== -1) {
		students.splice(index, 1);
		// res.json({ message: "Book deleted successfully" });
		res.status(204).send();
	} else {
		res.status(404).json({ message: "Student not found" });
	}

});

// Edit a student
server.put("/students/:id", (req: Request, res: Response) => {
	const studentId = req.params.id;
	const changes = req.body;

	const index = students.findIndex(student => student.id === studentId);

	if (index !== -1) {
		students[index] = changes;
		// res.json({ message: "Book edited successfully" });
		res.status(204).send();
	} else {
		res.status(404).json( { message: "Student does not exist"} );
	}

});

server.use(unknownEndpoint);

const port = 3000;
server.listen(port, () => {
	console.log("Server listening port", port);
});