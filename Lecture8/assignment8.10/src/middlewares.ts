import { Request, Response, NextFunction } from "express";

export const validate = (req: Request, res: Response, next: NextFunction ) => {
	if (!(req.body instanceof Array)) {
		return res.status(400).send("Missing or invalid parameters");
	}

	next();
};

// export const validatePut = (req: Request, res: Response, next: NextFunction ) => {
// 	if (!req.params.name && !req.params.score) {
// 		return res.status(400).send("Missing or invalid parameters");
// 	}
// 	next();
// };

export const unknownEndpoint = (_req: Request, res: Response) => {
	res.status(404).send({ error: "Not found"});
};