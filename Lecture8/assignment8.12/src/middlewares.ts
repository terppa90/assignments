import { Request, Response, NextFunction } from "express";

export const validate = (req: Request, res: Response, next: NextFunction ) => {
	const {id, name, author, read} = req.body;
	if (typeof(id) !== "number" || typeof(name) !== "string" || typeof(author) !== "string" || typeof(read) !== "boolean") {
		return res.status(400).send("Missing or invalid parameters");
	}
	next();
};

export const unknownEndpoint = (_req: Request, res: Response) => {
	res.status(404).send({ error: "404 Not found"});
};