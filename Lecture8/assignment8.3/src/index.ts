import express, { Request, Response } from "express";

const server = express();

server.listen(3000, () => {
	console.log("Listening to port 3000");
});

let count = 0;

server.get("/counter", (req: Request, res: Response) => {
	// count++;

	const number = Number(req.query.number);
	count = Number.isNaN(number) ? count++ : number;
	res.send({ count });

	// const resObj = { counter: count };
	// res.send(resObj);
});
