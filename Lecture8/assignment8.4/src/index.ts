import express, { Request, Response } from "express";

const server = express();

let counter = 0;

interface Counters {
	[key: string]: number
}

const counters: Counters = {};

server.get("/counter/:name", (req: Request, res: Response) => {
	const name = req.params.name;
	// if (counters[name] === undefined) {

	// }
	const number = Number(req.query.number);

	counters[name] += 1;

	counter = Number.isNaN(number) ? counter + 1 : number;
	// res.send({name});
	res.send(name + " was here " + counters[name] + " times");
});

const port = 3000;
server.listen(port, () => {
	console.log("Server listening port", port);
});