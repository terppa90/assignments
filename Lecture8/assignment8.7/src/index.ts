import express, { Request, Response } from "express";

// Import middlewares
import { loggerMiddleware } from "./middlewares";
import { unknownEndpoint } from "./middlewares";

const server = express();
server.use(express.json());

const students: string[] = [];

server.use(loggerMiddleware);

server.get("/students", (req: Request, res: Response) => {
	
	res.send(students);
});

server.post("/", (req: Request, res: Response) => {
	
	res.send(req.body);
});

server.use(unknownEndpoint);

const port = 3000;
server.listen(port, () => {
	console.log("Server listening port", port);
});