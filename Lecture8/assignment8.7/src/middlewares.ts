import { Request, Response, NextFunction } from "express";

export const loggerMiddleware = (req: Request, res: Response, next: NextFunction) => {
	// console.log(new Date());
	
	const time = new Date();
	const { method, url } = req.body;

	console.log(time);
	console.log(method);
	console.log(url);
	console.log(req.body);
	

	next();
};

export const unknownEndpoint = (_req: Request, res: Response) => {
	res.status(404).send({ error: "Not found"});
};