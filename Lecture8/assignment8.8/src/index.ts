import express, { Request, Response } from "express";

// Import middlewares
import { unknownEndpoint } from "./middlewares";
import { validate } from "./middlewares";

const server = express();
server.use(express.json());

interface Student {
	id: string,
	name: string,
	email: string
}

const students: Student[] = [];

server.post("/student", validate, (req: Request, res: Response) => {
	const student: Student = req.body;
	console.log(student.id);
	students.push(student);

	res.send(req.body);
});

// Get all students
server.get("/students", (req: Request, res: Response) => {
	// return only students id
	const getAllStudentsIds = students.map((student) => student.id);
	res.send(getAllStudentsIds);

	// return all
	// const getAllStudents = students.map((student => ({
	// 	...student
	// })));
	// res.send(getAllStudents);
});

// Get student by id
server.get("/student/:id", (req: Request, res: Response) => {
	const id = req.params.id;

	const getStudentById = students.find(student => student.id === id);

	if (getStudentById) {
		res.send(getStudentById);
	} else {
		res.status(404).json({ message: "User not found" });
	}
});

server.use(unknownEndpoint);

const port = 3000;
server.listen(port, () => {
	console.log("Server listening port", port);
});