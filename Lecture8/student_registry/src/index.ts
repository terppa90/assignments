import express, { Request, Response, NextFunction } from "express";

const server = express();

const students: string[] = [];

const loggerMiddleware = (req: Request, res: Response, next: NextFunction) => {
	const time = new Date();
	// const { method, url } = req;

	console.log(time);
	console.log(req.method);
	console.log(req.url);
	
	// res.send(`Time the request was made: ${time} | Method of the request: ${method} | url of the endpoint: ${url}`);

	next();
};

server.use(loggerMiddleware);

server.get("/students", (req: Request, res: Response) => {
	console.log(students);
	
	res.send({students});
});

const port = 3000;
server.listen(port, () => {
	console.log("Server listening port", port);
});