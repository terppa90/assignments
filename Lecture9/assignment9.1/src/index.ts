import express, { Request, Response } from "express";

// Import middlewares
import { unknownEndpoint } from "./middlewares";
import { validate } from "./middlewares";
import { validatePut } from "./middlewares";

const server = express();

server.use(express.json());
server.use(express.static("public"));

interface Student {
	id: string,
	name: string,
	email: string
}

const students: Student[] = [];

server.get("/", (req, res) => {
	res.send("OK");
});

server.post("/student", validate, (req: Request, res: Response) => {
	const student: Student = req.body;

	students.push(student);

	res.send(req.body);
});

// Get all students
server.get("/students", (req: Request, res: Response) => {
	// return only students id
	const getAllStudentsIds = students.map((student) => student.id);
	res.send(getAllStudentsIds);
});

// Get student by id
server.get("/student/:id", (req: Request, res: Response) => {
	const id = req.params.id;

	const getStudentById = students.find(student => student.id === id);

	if (getStudentById) {
		res.send(getStudentById);
	} else {
		res.status(404).json({ message: "Student not found" });
	}
});

// Delete a student by id
server.delete("/student/:id", (req: Request, res: Response) => {
	const id = req.params.id;

	const index = students.findIndex(student => student.id === id);

	if (index !== -1) {
		students.splice(index, 1);
		// res.json({ message: "Book deleted successfully" });
		res.status(204).send();
	} else {
		res.status(404).json({ message: "Student not found" });
	}

});

// Edit a student
server.put("/student/:id", validatePut, (req: Request, res: Response) => {
	const studentId = req.params.id;
	const changes = req.body;

	const index = students.findIndex(student => student.id === studentId);

	if (index !== -1) {
		students[index] = changes;
		// res.json({ message: "Book edited successfully" });
		res.status(204).send();
	} else {
		res.status(404).json( { message: "Student does not exist"} );
	}

});

server.use(unknownEndpoint);

const port = 3000;
server.listen(port, () => {
	console.log("Server listening port", port);
});