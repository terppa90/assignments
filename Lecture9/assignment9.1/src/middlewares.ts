import { Request, Response, NextFunction } from "express";

export const validate = (req: Request, res: Response, next: NextFunction ) => {
	const {id, name, email} = req.body;
	if (typeof(id) !== "string" || typeof(name) !== "string" || typeof(email) !== "string") {
		return res.status(400).send("Missing or invalid parameters");
	}
	next();
};

export const validatePut = (req: Request, res: Response, next: NextFunction ) => {
	if (!req.params.name && !req.params.email) {
		return res.status(400).send("Missing or invalid parameters");
	}
	next();
};

export const unknownEndpoint = (_req: Request, res: Response) => {
	res.status(404).send({ error: "Not found"});
};