import express, { Request, Response } from "express";
import usersRouter from "./usersRouter";
import "dotenv/config";

// Import middlewares
import { unknownEndpoint } from "./middlewares";
import { validate } from "./middlewares";

const server = express();
server.use(express.json());

// Routes
server.use("/api/v1/users", usersRouter);

interface Book {
	id: number,
	name: string,
	author: string,
	read: boolean
}

const books: Book[] = [];

server.post("/api/v1/books", validate, (req: Request, res: Response) => {
	const book: Book = req.body;

	books.push(book);

	res.send(req.body);
});

// Get all books
server.get("/api/v1/books", (req: Request, res: Response) => {
	const getAllBooks = books.map((book => ({
		...book
	})));
	res.send(getAllBooks);
});

// Get book by id
server.get("/api/v1/books/:id", (req: Request, res: Response) => {
	const id = Number(req.params.id);

	const getBookById = books.find(book => book.id === id);

	if (getBookById) {
		res.send(getBookById);
	} else {
		res.status(404).json({ message: "Book not found" });
	}
});

// Delete a book by id
server.delete("/api/v1/books/:id", (req: Request, res: Response) => {
	const id = Number(req.params.id);

	const index = books.findIndex(book => book.id === id);

	if (index !== -1) {
		books.splice(index, 1);
		res.json({ message: "Book deleted successfully" });
	} else {
		res.status(404).json({ message: "Book not found" });
	}

});

// Edit a book
server.put("/api/v1/books/:id", validate, (req: Request, res: Response) => {
	const BookId = Number(req.params.id);
	const changes = req.body;

	const index = books.findIndex(book => book.id === BookId);

	if (index !== -1) {
		books[index] = changes;
		res.status(200).json(books[index]);
	} else {
		res.status(404).json( { message: "Book does not exist"} );
	}

});

server.use(unknownEndpoint);

const port = 3000;
server.listen(port, () => {
	console.log("Server listening port", port);
});