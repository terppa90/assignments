import { Request, Response, NextFunction } from "express";
import jwt from "jsonwebtoken";

interface CustomRequest extends Request {
    username?: string | jwt.JwtPayload
}

export const validate = (req: Request, res: Response, next: NextFunction ) => {
	const {id, name, author, read} = req.body;
	if (typeof(id) !== "number" || typeof(name) !== "string" || typeof(author) !== "string" || typeof(read) !== "boolean") {
		return res.status(400).send("Missing or invalid parameters");
	}
	next();
};

export const validateUserPost = (req: Request, res: Response, next: NextFunction) => {
	const { username, password} = req.body;

	if (typeof username !== "string" || typeof password !== "string") {
		return res.status(400).send("Missing username and/or password");
	}

	if (username === "" || password === "") {
		return res.status(400).send("Invalid username and/or password");
	}
	next();
};

export const authenticate = (req: CustomRequest, res: Response, next: NextFunction) => {
	const auth = req.get("Authorization");
	console.log(auth);
	
	if (!auth?.startsWith("Bearer ")) {
		return res.status(401).send("Invalid token");
	}
	const token = auth.substring(7);
	const secret = process.env.SECRET ?? "";
	try {
		const decodedToken = jwt.verify(token, secret);
		console.log(decodedToken);
		
		req.username = decodedToken;
		next();
	} catch (error) {
		return res.status(401).send("Invalid token");
	}
};

export const unknownEndpoint = (_req: Request, res: Response) => {
	res.status(404).send({ error: "404 Not found"});
};

// https://docs.insomnia.rest/insomnia/authentication