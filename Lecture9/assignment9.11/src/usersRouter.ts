import express, { Request, Response } from "express";
import argon2 from "argon2";
import jwt from "jsonwebtoken";

const router = express.Router();

interface User {
    username: string,
    hash: string
}

let users: Array<User> = [];

router.post("/register", async (req: Request, res: Response) => {
	const { username, password } = req.body;

	const existingUser = users.find(user => user.username === username);

	if (existingUser) {
		res.status(401).send("Username already exists");
	}

	if (!username || !password) {
		res.status(401).send("Username or password missing");
	}

	if(!existingUser) {
		const hash = await argon2.hash(password);
		const user: User = { username, hash };

		users = users.concat(user);
		console.log(user);

		const payload = { username: user.username };
		const secret = process.env.SECRET ?? "";
		const options = { expiresIn: "15min"};

		const encodedToken = jwt.sign(payload, secret, options);
		// console.log(encodedToken);
		res.status(201).send(encodedToken);
	}
	

	res.status(201).send();
});

router.post("/login", async (req: Request, res: Response) => {
	const { username, password } = req.body;

	const existingUser = users.find(user => user.username === username);

	if (existingUser === undefined) {
		return res.status(401).send("Invalid username");
	}

	if(existingUser) {
		const isValidPassword = await argon2.verify(existingUser.hash, password);
		if (!isValidPassword) {
			return res.status(401).send("Invalid password");
		}

		const payload = { username: username };
		const secret = process.env.SECRET ?? "";
		const options = { expiresIn: "15min"};

		const encodedToken = jwt.sign(payload, secret, options);
		// console.log(encodedToken);
		return res.status(201).send(encodedToken);
	}

	res.status(204).send();

});

router.post("/admin", async (req: Request, res: Response) => {
	const { username, password } = req.body;
	const { ADMIN_HASHED_PASSWORD, ADMIN_USERNAME} = process.env;

	if (ADMIN_HASHED_PASSWORD === undefined || ADMIN_USERNAME === undefined) {
		console.error("Missing admin credentials.");
		res.status(500).send("Server error");
		return;
	}

	const isPasswordCorrect = await argon2.verify(ADMIN_HASHED_PASSWORD, password);

	if (username !== ADMIN_USERNAME || !isPasswordCorrect) {
		return res.status(401).send();
	}

	const payload = { username: username, isAdmin:true };
	const secret = process.env.SECRET ?? "";
	const options = { expiresIn: "15min"};

	const token = jwt.sign(payload, secret, options);
	console.log(token);
	return res.status(200).json({ token });

	// try {
	// 	const decodedToken = jwt.verify(encodedToken, secret);
	// 	return res.status(200).json({decodedToken});
	// 	console.log(decodedToken);
		
	// } catch (error) {
	// 	console.log("Error:" + error);
	// }

	res.status(204).send();

});

export default router;