import request from "supertest";
import server from "../src/server";

interface User {
    username: string,
    hash: string
}

describe("Server", () => {
	it("Returns 404 on invalid address", async () => {
		const response = await request(server)
			.get("/invalidaddress");
		expect(response.statusCode).toBe(404);
	});
});

describe("POST /register", () => {
	it("Returns 201 on succesful register", async () => {
		const user = { username: "John", password: "Mypassword"};

		const response = await request(server)
			.post("/api/v1/users/register").send(user);
		expect(response.statusCode).toBe(201);
		expect(response.body).toBeDefined();
	});

	it("Returns 401 if username is undefined", async () => {
		const user = { password: "Mypassword" };

		const response = await request(server)
			.post("/api/v1/users/register").send(user);
		expect(response.statusCode).toBe(401);
	});

	it("Returns 401 if password is undefined", async () => {
		const user = { username: "John" };

		const response = await request(server)
			.post("/api/v1/users/register").send(user);
		expect(response.statusCode).toBe(401);
	});

	it("Returns 401 if username already exists", async () => {
		const user = { username: "John", password: "Mypassword"};

		const users: Array<User> = [];

		const existingUser = users.find(elem => elem.username === user.username);

		const response = await request(server)
			.post("/api/v1/users/register").send(existingUser);
		expect(response.statusCode).toBe(401);
	});
});

describe("POST /login", () => {
	it("Returns 201 on succesful login", async () => {
		const user = { username: "John", password: "Mypassword"};

		const response = await request(server)
			.post("/api/v1/users/login").send(user);
		expect(response.statusCode).toBe(201);
		expect(response.body).toBeDefined();
	});

	it("Returns 401 on invalid username", async () => {
		const user = { username: "Jane", password: "Mypassword"};

		const users: Array<User> = [];

		const existingUser = users.find(elem => elem.username === user.username);

		const response = await request(server)
			.post("/api/v1/users/login").send(existingUser);
		expect(response.statusCode).toBe(401);
		expect(response.body).toBeDefined();
	});

	it("Returns 401 on invalid password", async () => {
		const user = { username: "John", password: "Wrongpassword"};

		const users: Array<User> = [];

		const existingUser = users.find(elem => elem.username === user.username);

		const response = await request(server)
			.post("/api/v1/users/login").send(existingUser);
		expect(response.statusCode).toBe(401);
		expect(response.body).toBeDefined();
	});
});

describe("POST /admin", () => {
	it("Returns 401 if username is not equal to admin username", async () => {
		const user = { username: "John", password: "Mypassword"};

		const response = await request(server)
			.post("/api/v1/users/admin").send(user);
		expect(response.statusCode).toBe(401);
		expect(response.body).toBeDefined();
	});

	it("Returns 200 if username and password is equal to admin username/password", async () => {
		const user = { username: "tero", password: "mypassword123"};

		const response = await request(server)
			.post("/api/v1/users/admin").send(user);
		expect(response.statusCode).toBe(200);
		expect(response.body).toBeDefined();
	});
});


// https://medium.com/@csalazar94/javascript-testing-made-easy-a-step-by-step-guide-with-jest-and-supertest-8e2a35f13506
// https://curric.rithmschool.com/springboard/lectures/express-jest-testing/