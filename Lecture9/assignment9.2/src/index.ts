import express from "express";
import studentRouter from "./studentRouter";

// Import middlewares
import { unknownEndpoint } from "./middlewares";

const server = express();

server.use(express.json());
server.use(express.static("public"));
// Routes
server.use("/students", studentRouter);

server.use(unknownEndpoint);

const port = 3000;

server.listen(port, () => {
	console.log("Server listening port", port);
});