import express, { Request, Response } from "express";
import argon2 from "argon2";

import { validateUserPost } from "./middlewares";

const router = express.Router();

interface User {
	username: string,
	password: string,
}

let users: Array<User> = [];

router.post("/register", validateUserPost, async (req: Request, res: Response) => {
	const hash = await argon2.hash(req.body.password);
	console.log("hash", hash);

	const user: User = { username: req.body.username, password: hash};
	users = users.concat(user);
	console.log(user);
    
	res.status(201).send();
});


export default router;