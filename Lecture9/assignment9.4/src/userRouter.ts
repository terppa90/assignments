import express, { Request, Response } from "express";
import argon2 from "argon2";

const router = express.Router();

interface User {
    username: string,
    hash: string
}

let users: Array<User> = [];

router.post("/register", async (req: Request, res: Response) => {
	const { username, password } = req.body;
	if (!username || !password) {
		res.status(400).send();
	}

	const hash = await argon2.hash(password);
	const user: User = { username, hash };

	users = users.concat(user);
	console.log(user);

	res.status(201).send();
});

router.post("/login", async (req: Request, res: Response) => {
	const { username, password } = req.body;

	const existingUser = users.find(user => user.username === username);
	if (existingUser === undefined) {
		return res.status(401).send("Invalid username or password");
	}

	const isValidPassword = await argon2.verify(existingUser.hash, password);
	if (!isValidPassword) {
		return res.status(401).send("Invalid username or password");
	}

	res.status(204).send();

});

export default router;