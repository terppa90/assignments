import express from "express";
import "dotenv/config";
import jwt from "jsonwebtoken";

const server = express();

server.use(express.json());
server.use(express.static("public"));

const payload = { username: "John" };
const secret = process.env.SECRET ?? "";
const options = { expiresIn: "15min"};

const token = jwt.sign(payload, secret, options);
console.log(token);

const port = 3000;
server.listen(port, () => {
	console.log("Server listening port", port);
});