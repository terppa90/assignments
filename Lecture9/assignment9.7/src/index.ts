import express from "express";
import "dotenv/config";
import jwt from "jsonwebtoken";

const server = express();

server.use(express.json());

const payload = { username: "JohnDoe" };
const secret = process.env.SECRET ?? "";
const options = { expiresIn: "15min"};

const encodedToken = jwt.sign(payload, secret, options);
console.log(encodedToken);

// Verify the token
try {
	const decodedToken = jwt.verify(encodedToken, secret);
	console.log(decodedToken);
	
} catch (error) {
	console.log("Error:" + error);
}

const port = 3000;
server.listen(port, () => {
	console.log("Server listening port", port);
});

// https://jwt.io/