import express from "express";
import studentRouter from "./studentRouter";
import userRouter from "./userRouter";
import jwt from "jsonwebtoken";
import "dotenv/config";

// Create a token
const payload = { username: "JohnDoe" };
const secret = process.env.SECRET ?? "";
const options = { expiresIn: "15min"};

const token = jwt.sign(payload, secret, options);
console.log(token);

// Import middlewares
import { unknownEndpoint } from "./middlewares";

const server = express();

server.use(express.json());
server.use(express.static("public"));
// Routes
server.use("/students", studentRouter);
server.use("/users", userRouter);

server.use(unknownEndpoint);

const port = 3000;

server.listen(port, () => {
	console.log("Server listening port", port);
});