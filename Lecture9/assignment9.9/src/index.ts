// import express from "express";
import server from "./server";
import "dotenv/config";

const port = 3000;

server.listen(port, () => {
	console.log("Server listening port", port);
});