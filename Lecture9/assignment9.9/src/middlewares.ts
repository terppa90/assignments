import { Request, Response, NextFunction } from "express";
import jwt from "jsonwebtoken";

export const validate = (req: Request, res: Response, next: NextFunction ) => {
	const {id, name, email} = req.body;
	if (typeof(id) !== "string" || typeof(name) !== "string" || typeof(email) !== "string") {
		return res.status(400).send("Missing or invalid parameters");
	}
	next();
};

export const validatePut = (req: Request, res: Response, next: NextFunction ) => {
	const { name, email} = req.body;

	if (!name && !email) {
		return res.status(400).send("Missing or invalid parameters");
	}
	next();
};

export const validateUserPost = (req: Request, res: Response, next: NextFunction) => {
	const { username, password} = req.body;

	if (typeof username !== "string" || typeof password !== "string") {
		return res.status(400).send("Missing username and/or password");
	}

	if (username === "" || password === "") {
		return res.status(400).send("Invalid username and/or password");
	}
	next();
};

export const unknownEndpoint = (_req: Request, res: Response) => {
	res.status(404).send({ error: "Not found"});
};

interface CustomRequest extends Request {
    user?: string | jwt.JwtPayload
}

export const authenticate = (req: CustomRequest, res: Response, next: NextFunction) => {
	const auth = req.get("Authorization");
	if (!auth?.startsWith("Bearer ")) {
		return res.status(401).send("Invalid token");
	}
	const token = auth.substring(7);
	const secret = process.env.SECRET ?? "";
	try {
		const decodedToken = jwt.verify(token, secret);
		req.user = decodedToken;
		next();
	} catch (error) {
		return res.status(401).send("Invalid token");
	}
};
