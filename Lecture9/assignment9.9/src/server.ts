import express from "express";
import studentRouter from "./studentRouter";
import userRouter from "./userRouter";

const server = express();

// Import middlewares
import { unknownEndpoint } from "./middlewares";

server.use(express.json());
server.use(express.static("public"));
// Routes
server.use("/students", studentRouter);
server.use("/users", userRouter);

server.use(unknownEndpoint);


export default server;