import express, { Request, Response } from "express";

import { validate, validatePut, authenticate } from "./middlewares";
// import { validatePut } from "./middlewares";

const router = express.Router();

interface Student {
	id: string,
	name: string,
	email: string
}

const students: Student[] = [];

// router.get("/", (req, res) => {
// 	res.send("OK");
// });

router.post("/", validate, authenticate, (req: Request, res: Response) => {
	const student: Student = req.body;

	students.push(student);

	// res.send(req.body);
	res.status(200).send(req.body);
});

// Get all students
router.get("/", authenticate, (req: Request, res: Response) => {
	// return only students id
	const getAllStudentsIds = students.map((student) => student.id);
	res.send(getAllStudentsIds);
});

// Get student by id
router.get("/:id", authenticate, (req: Request, res: Response) => {
	const id = req.params.id;

	const getStudentById = students.find(student => student.id === id);

	if (getStudentById) {
		res.send(getStudentById);
	} else {
		res.status(404).json({ message: "Student not found" });
	}
});

// Delete a student by id
router.delete("/:id", authenticate, (req: Request, res: Response) => {
	const id = req.params.id;

	const index = students.findIndex(student => student.id === id);

	if (index !== -1) {
		students.splice(index, 1);
		// res.json({ message: "Book deleted successfully" });
		res.status(204).send();
	} else {
		res.status(404).json({ message: "Student not found" });
	}

});

// Edit a student
router.put("/:id", validatePut, authenticate, (req: Request, res: Response) => {
	const studentId = req.params.id;
	const changes = req.body;

	const index = students.findIndex(student => student.id === studentId);

	if (index !== -1) {
		students[index] = changes;
		// res.json({ message: "Book edited successfully" });
		res.status(204).send();
	} else {
		res.status(404).json( { message: "Student does not exist"} );
	}

});

export default router;