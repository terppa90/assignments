import express, { Request, Response } from "express";
import argon2 from "argon2";

import { validateUserPost } from "./middlewares";

const router = express.Router();

interface User {
	username: string,
	password: string,
}

let users: Array<User> = [];

router.post("/register", validateUserPost, async (req: Request, res: Response) => {
	const hash = await argon2.hash(req.body.password);
	console.log("hash", hash);

	const user: User = { username: req.body.username, password: hash};
	users = users.concat(user);
	console.log(user);
    
	res.status(201).send();
});

router.post("/login", async (req: Request, res: Response) => {
	const { username, password } = req.body;
	console.log(username, password);
	
	const user = users.find(user => user.username === req.body.username);

	if (user === undefined) {
		return res.status(401).send("Invalid username and/or password");
	}

	const isCorrectPass = await argon2.verify(user.password, req.body.password);

	if (!isCorrectPass) {
		return res.status(401).send("Invalid username and/or password");
	}
	
	res.status(204).send;
	
});

router.post("/admin", async (req: Request, res: Response) => {
	const { username, password } = req.body;
	const { ADMIN_PASSWORD, ADMIN_USERNAME} = process.env;

	if (ADMIN_USERNAME === undefined || ADMIN_PASSWORD === undefined) {
		console.error("Missing admin credentials.");
		res.status(500).send("Server error");
		return;
	}

	// Toinen tapa
	// const adminUsername = process.env.ADMIN_USERNAME ?? "";
	// const adminHash = process.env.ADMIN_PASSWORD ?? "";


	const isPasswordCorrect = await argon2.verify(ADMIN_PASSWORD, password);

	if (username !== ADMIN_USERNAME || !isPasswordCorrect) {
		return res.status(401).send();
	}

	res.status(204).send();
});

export default router;