import request from "supertest";
import server from "../src/server";

describe("Server", () => {
	it("Returns 404 on invalid address", async () => {
		const response = await request(server)
			.get("/invalidaddress");
		expect(response.statusCode).toBe(404);
	});

	
	it("Returns 201 on valid address", async () => {
		const user = { username: "John", password: "Mypassword"};

		const response = await request(server)
			.post("/users/register").send(user);
		expect(response.statusCode).toBe(201);
		expect(response.body).toBeDefined();
	});
});
